<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }        
    }

    public function insertCliente($salt, $CF, $nome, $cognome, $Email, $Telefono, $Nome_Utente, $Password){
        $query = "INSERT INTO CLIENTE (CF, Nome, Cognome, Email, Telefono, Nome_Utente, Password, Salt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssssss',$CF, $nome, $cognome, $Email, $Telefono, $Nome_Utente, $Password, $salt);
        $stmt->execute();
        return $stmt->error;
    }

    public function insertCorriere($salt, $P_IVA, $Email, $Telefono, $Nome_Utente, $Password){
        $query = "INSERT INTO CORRIERE (P_IVA, Email, Telefono, Nome_Utente, Password, Salt) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssssss',$P_IVA, $Email, $Telefono, $Nome_Utente, $Password, $salt);
        $stmt->execute();
        return $stmt->error;
    }

    public function insertVenditore($salt, $P_IVA, $Casa_Asta, $Email, $Telefono, $Nome_Utente, $Password, $logo, $copertina){
        $query = "INSERT INTO VENDITORE (P_IVA, Casa_Asta, Email, Telefono, Nome_Utente, Password, Salt, Link_logo, Link_ImmCopertina) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sisssssss',$P_IVA, $Casa_Asta, $Email, $Telefono, $Nome_Utente, $Password, $salt, $logo, $copertina);
        $stmt->execute();
        return $stmt->error;
    }

    public function updateLogo($nomeutente, $logo){
        $query = "UPDATE VENDITORE 
                SET Link_logo = ?
                WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $logo, $nomeutente);
        return $stmt->execute();
    }

    public function updateCopertina($nomeutente, $copertina){
        $query = "UPDATE VENDITORE 
                SET Link_ImmCopertina = ?
                WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $copertina, $nomeutente);
        return $stmt->execute();
    }

    public function getSalt($username)
    {
        $cliente = $this->checkSalt($username, "CLIENTE");
        if(count($cliente)!=0){
            return $cliente;
        } else {
            $venditore = $this->checkSalt($username, "VENDITORE");
            if(count($venditore)!=0){
                return $venditore;
            } else {
                $corriere = $this->checkSalt($username, "CORRIERE");
                if(count($corriere)!=0){
                    return $corriere;
                } else {
                    return $cliente;
                }
            }
        }
    }

    private function checkSalt($username, $tipo)
    {
        if($tipo == "CLIENTE"){
            $query = "SELECT Salt FROM ".$tipo." WHERE CF = ?";
        } else {
            $query = "SELECT Salt FROM ".$tipo." WHERE P_IVA = ?";
        }
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function checkLogin($username, $password){
        $cliente = $this->checkLoginCliente($username, $password);
        if(count($cliente)!=0){
            $cliente[0]["tipo"] = "CLIENTE";
            return $cliente;
        } else {
            $venditore = $this->checkLoginVenditore($username, $password);
            if(count($venditore)!=0){
                $venditore[0]["tipo"] = "VENDITORE";
                $cliente[0]["CF"] = "";
                return $venditore;
            } else {
                $corriere = $this->checkLoginCorriere($username, $password);
                if(count($corriere)!=0){
                    $corriere[0]["tipo"] = "CORRIERE";
                    $cliente[0]["CF"] = "";
                    return $corriere;
                } else {
                    return $cliente;
                }
            }
        }       
    }

    private function checkLoginCliente($username, $password){
        $query = "SELECT Nome_Utente, CF FROM CLIENTE WHERE CF = ? AND Password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    private function checkLoginVenditore($username, $password){
        $query = "SELECT Nome_Utente, Casa_Asta FROM VENDITORE WHERE P_IVA = ? AND Password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    private function checkLoginCorriere($username, $password){
        $query = "SELECT Nome_Utente FROM CORRIERE WHERE P_IVA = ? AND Password = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $username, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMyIndirizzo($tipo, $nomeutente){
        $query = "SELECT Via, N_Civico, Citta, Provincia, CAP FROM ".$tipo." WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nomeutente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMySpedizioni($CF){
        $query = "SELECT * FROM SPEDIZIONE WHERE CF = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $CF);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateIndirizzo($tipo, $nomeutente, $Via, $N_Civico, $Citta, $Provincia, $CAP){
        $query = "UPDATE ".$tipo." 
                SET Via = ?, N_Civico = ?, Citta = ?, Provincia = ?, CAP = ? 
                WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sissis', $Via, $N_Civico, $Citta, $Provincia, $CAP, $nomeutente);
        return $stmt->execute();       
    }

    public function updatePassword($tipo, $nomeutente, $Password, $salt){
        $query = "UPDATE ".$tipo." 
                SET Password = ?, Salt = ? 
                WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $Password, $salt, $nomeutente);
        return $stmt->execute();       
    }

    public function getRandomFotoAuto($n){
        $stmt = $this->db->prepare("SELECT IdAuto, Modello, Link_immagine FROM AUTO ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomVenditori($n){
        $stmt = $this->db->prepare("SELECT Nome_Utente, Link_logo FROM VENDITORE WHERE Casa_Asta = 0 ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllVenditori(){
        $stmt = $this->db->prepare("SELECT Nome_Utente, Link_logo FROM VENDITORE WHERE Casa_Asta = 0");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    public function getLastWish($CF, $n){
        $stmt = $this->db->prepare("SELECT A.IdAuto, A.Modello, A.Link_immagine, A.Prezzo_base FROM LISTA_DESIDERI as L INNER JOIN AUTO as A ON L.IdAuto = A.IdAuto WHERE CF = '".$CF."' LIMIT ?");
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function loginToChangePassword($tipo, $id, $email, $nomeutente){
        if($tipo == "Cliente"){
            $query = "SELECT Nome_Utente FROM ".$tipo." WHERE CF = ? AND Nome_Utente = ? AND Email = ?";
        } else {
            $query = "SELECT Nome_Utente FROM ".$tipo." WHERE P_IVA = ? AND Nome_Utente = ? AND Email = ?";
        }
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $id, $nomeutente, $email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getModelscontains($search){
        $param = "%{$search}%";
        $stmt = $this->db->prepare("SELECT IdAuto, Modello, Prezzo_base FROM AUTO WHERE Modello LIKE ?");
        $stmt->bind_param('s', $param);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getIdMetodoPagamento($CF, $Fatturazione)
    {
        $query = "SELECT IdMetodoPagamento FROM METODO_PAGAMENTO WHERE CF = ? AND Fatturazione = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $CF, $Fatturazione);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertMetodoPagamento($CF, $Fatturazione)
    {
        $query = "INSERT INTO METODO_PAGAMENTO (CF, Fatturazione) VALUES (?, ?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss', $CF, $Fatturazione);
        $stmt->execute();
        return $stmt->error;      
    }    

    public function insertAndgetIdPagamento($IdMetodoPagamento)
    {
        $query = "INSERT INTO PAGAMENTO (IdMetodoPagamento) VALUES (?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $IdMetodoPagamento);
        $stmt->execute();
        if($stmt->error==""){
            $query = "SELECT LAST_INSERT_ID() as IdPagamento;";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return $stmt->error;
    }

    public function updateStatoOrdineToRichiesta($IdOrdine, $IdPagamento, $IdSpedizione, $P_IVA_Corriere)
    {
        $query = "UPDATE ORDINE 
                SET IdPagamento = ?, IdStato  = 2, IdSpedizione = ?, P_IVA = ? 
                WHERE IdOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iisi', $IdPagamento, $IdSpedizione, $P_IVA_Corriere, $IdOrdine);
        return $stmt->execute();  
    }

    public function checkOrdineGiaPagato($IdOrdine){
        $query = "SELECT IdPagamento FROM ORDINE WHERE IdOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $IdOrdine);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateSpedizione($IdSpedizione, $Via, $N_Civico, $Citta, $Provincia, $CAP){
        $query = "UPDATE SPEDIZIONE 
                SET Via = ?, N_Civico = ?, Citta = ?, Provincia = ?, CAP = ? 
                WHERE IdSpedizione = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sissii', $Via, $N_Civico, $Citta, $Provincia, $CAP, $IdSpedizione);
        return $stmt->execute();       
    }

    public function insertSpedizione($CF, $Via, $N_Civico, $Citta, $Provincia, $CAP){
        $query = "INSERT INTO SPEDIZIONE (CF, Via, N_Civico, Citta, Provincia, CAP) VALUES (?, ?, ?, ?, ?, ?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssissi', $CF, $Via, $N_Civico, $Citta, $Provincia, $CAP); 
        $stmt->execute();
        return $stmt->error;            
    }

    public function getMyOrdiniCorriere($nomeutente){
        $query = "SELECT P_IVA FROM CORRIERE WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nomeutente);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $P_IVACorriere = $result[0]["P_IVA"];

        $query = "SELECT IdOrdine, IdStato FROM ORDINE WHERE P_IVA = ? AND IdPagamento IS NOT NULL AND IdStato != 6";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $P_IVACorriere);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllStatiOrdine(){
        $query = "SELECT * FROM STATO_ORDINE";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function updateStatusOrdine($IdOrdine, $IdStato){
        $query = "UPDATE ORDINE 
                SET IdStato  = ?
                WHERE IdOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $IdStato, $IdOrdine);
        return $stmt->execute();
    }

    public function insertNotifica($IdOrdine, $CF, $Messaggio, $Titolo){
        $query = "INSERT INTO NOTIFICHE (IdOrdine, IdPrimario, Messaggio, Titolo) VALUES (?, ?, ?, ?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isss', $IdOrdine, $CF, $Messaggio, $Titolo); 
        $stmt->execute();
        return $stmt->error;  
    }

    public function insertNotificaCorriere($IdOrdine, $P_IVA, $Messaggio, $Titolo){
        $query = "INSERT INTO NOTIFICHE_CORRIERE (IdOrdine, P_IVA, Messaggio, Titolo) VALUES (?, ?, ?, ?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isss', $IdOrdine, $P_IVA, $Messaggio, $Titolo); 
        $stmt->execute();
        return $stmt->error;  
    }

    public function insertNotificaVenditore($IdOrdine, $P_IVA, $Messaggio, $Titolo){
        $query = "INSERT INTO NOTIFICHE_VENDITORE (IdOrdine, P_IVA, Messaggio, Titolo) VALUES (?, ?, ?, ?);";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isss', $IdOrdine, $P_IVA, $Messaggio, $Titolo); 
        $stmt->execute();
        return $stmt->error;  
    }

    public function getCFNomeUtenteFromOrdine($IdOrdine)
    {
        $query = "SELECT C.Nome_Utente, C.CF FROM AUTO_CONFIGURATA as A INNER JOIN CLIENTE as C ON A.CF = C.CF WHERE IdOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $IdOrdine); 
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEmailFromClienteCF($CF)
    {
        $query = "SELECT Email FROM CLIENTE WHERE CF = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $CF); 
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPrezzoTotaleOrdine($IdOrdine, $CF)
    {
        $query = "SELECT sum(`PrezzoTotale`) as PrezzoTotale FROM `auto_configurata` WHERE `IdOrdine` = ? AND `CF` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $IdOrdine, $CF); 
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllMyCar($nomeutente) {
        $query = "SELECT AUTO.IdAuto, AUTO.Modello, AUTO.Link_immagine FROM AUTO INNER JOIN GESTISCE on AUTO.IdAuto = GESTISCE.IdAuto INNER JOIN VENDITORE ON GESTISCE.P_IVA = VENDITORE.P_IVA WHERE VENDITORE.Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nomeutente); 
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNomeUtentePIVAEmailCorriereFromIdOrdine($IdOrdine){
        $query = "SELECT C.P_IVA, C.Nome_Utente, C.Email FROM ordine AS O INNER JOIN corriere AS C ON O.P_IVA = C.P_IVA WHERE `IdOrdine` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $IdOrdine); 
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        if(count($result) != 0){
            return $result[0];
        } else {
            return NULL;
        }
    }

    public function getNomeUtentePIVAEmailVenditoreFromIdOrdine($IdOrdine){
        $query = "SELECT V.P_IVA, V.Nome_Utente, V.Email FROM ordine AS O 
                    INNER JOIN auto_configurata AS autoC ON O.IdOrdine = autoC.IdOrdine 
                    INNER JOIN gestisce AS gest ON autoC.IdAuto = gest.IdAuto 
                    INNER JOIN venditore AS V ON gest.P_IVA = V.P_IVA 
                    WHERE O.IdOrdine = ? 
                    GROUP BY V.P_IVA, V.Nome_Utente";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $IdOrdine); 
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllCorrieri() {
        $query = "SELECT P_IVA, Nome_Utente FROM CORRIERE";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DILO function to manage notifiche!!!!
    public function getNumeroNotifiche($cf, $letto = 0){
        $query = "SELECT COUNT(*) FROM notifiche WHERE IdPrimario = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf, $letto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = 0;
        if(isset($result[0]["COUNT(*)"])){
            $num = $result[0]["COUNT(*)"];
        } else {
            $num = 0;
        }        
        return $num;
    }
}
?>