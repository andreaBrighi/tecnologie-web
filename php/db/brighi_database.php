<?php
class DatabaseHelperBrighi{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }        
    }

    public function getPreferiti($CF,$venditore){
        $query = "SELECT D.*, L.Modello, L.IdAuto, L.Link_immagine,L.Link_ImmCopertina FROM (SELECT * FROM LISTA_DESIDERI as LD WHERE LD.CF = '".$CF."' ) as D RIGHT JOIN (SELECT A.Modello, A.IdAuto, A.Link_immagine, V.Link_ImmCopertina FROM VENDITORE AS V INNER JOIN `Gestisce` AS G on (V.P_IVA = G.P_IVA) INNER JOIN AUTO AS A ON (A.IdAuto = G.IdAuto)
        WHERE V.Nome_Utente ='".$venditore."') as L on (L.IdAuto = D.IdAuto)";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getVenditore($venditore){
        $query = "SELECT * FROM VENDITORE as V WHERE v.Nome_Utente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$venditore);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRecensioni($venditore, $n=10){
        $query = "SELECT C.*, A.Modello, CL.Nome_Utente
        FROM VENDITORE AS V INNER JOIN `Gestisce` AS G on (V.P_IVA = G.P_IVA) INNER JOIN AUTO AS A ON (A.IdAuto = G.IdAuto) inner JOIN `COMMENTO` AS C ON (C.IdAuto =A.IdAuto) INNER JOIN CLIENTE AS CL ON (CL.CF=C.CF)
        WHERE V.Nome_Utente=? 
        LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$venditore, $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRecensioniMedia($venditore){
        $query = "SELECT AVG(NumStelline) AS Media
        FROM VENDITORE AS V INNER JOIN `Gestisce` AS G on (V.P_IVA = G.P_IVA) INNER JOIN AUTO AS A ON (A.IdAuto = G.IdAuto) inner JOIN `COMMENTO` AS C ON (C.IdAuto =A.IdAuto) INNER JOIN CLIENTE AS CL ON (CL.CF=C.CF)
        WHERE V.Nome_Utente=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$venditore);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function addPreferiti($modello, $cf){
        $query = "SELECT * FROM `LISTA_DESIDERI` WHERE CF=? AND IdAuto=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$cf, $modello);
        $stmt->execute();
        $result = $stmt->get_result();
        if(count($result->fetch_all(MYSQLI_ASSOC))==0){
            $query = "INSERT INTO `LISTA_DESIDERI` (CF, IdAuto) VALUES (?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ss',$cf, $modello);
            $stmt->execute();
        }else{
            $query = "DELETE FROM `LISTA_DESIDERI` WHERE CF=? AND IdAuto=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ss',$cf, $modello);
            $stmt->execute();
        }

    }

    public function getConfig($id){
        $query = "SELECT * 
                    FROM `AUTO_CONFIGURATA` as AC inner join `AUTO` as A on(A.IdAuto = AC.IdAuto)
                    WHERE IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAuto($id){
        $query = "SELECT * 
                    FROM `AUTO`
                    WHERE IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTitleInterni($id){
        $query = "SELECT Descrizione
                    FROM `INTERNI`
                    WHERE IdAuto=?
                    GROUP BY Descrizione;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTitleEsterni($id){
        $query = "SELECT Descrizione
                    FROM `ESTERNI`
                    WHERE IdAuto=?
                    GROUP BY Descrizione;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMotori($id){
        $query = "SELECT * 
                    FROM `MOTORE`
                    WHERE IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getInterni($id){
        $query = "SELECT * 
                    FROM `INTERNI`
                    WHERE IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEsterni($id){
        $query = "SELECT * 
                    FROM `ESTERNI`
                    WHERE IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOptionals($id){
        $query = "SELECT * 
                    FROM `OPTIONAL`
                    WHERE IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMotore($id, $auto){
        $query = "SELECT * 
                    FROM `MOTORE`
                    WHERE IdMotore=? and IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $id, $auto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getInterno($id, $auto){
        $query = "SELECT * 
                    FROM `INTERNI`
                    WHERE IdInterni=? and IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $id, $auto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEsterno($id, $auto){
        $query = "SELECT * 
                    FROM `ESTERNI`
                    WHERE IdEsterni=? and IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $id, $auto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOptional($id, $auto){
        $query = "SELECT * 
                    FROM `OPTIONAL`
                    WHERE IdOptional=? and IdAuto=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $id, $auto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getInterniScelti($id){
        $query = "SELECT * 
                    FROM `INTERNO_SCELTO`
                    WHERE IdAutoConfigurata=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEsterniScelti($id){
        $query = "SELECT * 
                    FROM `ESTERNI_SCELTE`
                    WHERE IdAutoConfigurata=?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOptionalScelti($id){
        $query = "SELECT * 
                    FROM `OPTIONAL_SCELTO`
                    WHERE IdAutoConfigurata = ?;";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insertConfig( $idAuto, $motore, $cf, $tot,$id=null){
        if($id!=null){
            $query = "UPDATE `AUTO_CONFIGURATA`
                    SET IdMotore = ?, PrezzoTotale = ? 
                    WHERE IdAutoConfigurata = ? ";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('iii',$motore, $tot, $id);
            $stmt->execute();
            $query = "DELETE 
                    FROM `OPTIONAL_SCELTO`
                    WHERE IdAutoConfigurata=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $query = "DELETE  
                    FROM `INTERNO_SCELTO`
                    WHERE IdAutoConfigurata=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $query = "DELETE 
                    FROM `ESTERNI_SCELTE`
                    WHERE IdAutoConfigurata=?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i', $id);
            $stmt->execute();
            return $id;
        }
        else{
            $query = "INSERT INTO `AUTO_CONFIGURATA` (CF, IdAuto, IdMotore,PrezzoTotale ) 
                        VALUES (?, ?, ?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('siii', $cf,$idAuto, $motore, $tot);
            $stmt->execute();
            return $this->db -> insert_id;
        }
    }

    public function insertInterni($id, $interno, $idAuto){
        $query = "INSERT INTO `INTERNO_SCELTO` (IdAutoConfigurata, IdAuto, IdInterni) 
                        VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sii', $id,$idAuto, $interno);
        $stmt->execute();
    }

    public function insertEsterni($id, $esterno, $idAuto){
        $query = "INSERT INTO `ESTERNI_SCELTE` (IdAutoConfigurata, IdAuto, IdEsterni) 
                        VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sii', $id,$idAuto, $esterno);
        $stmt->execute();
    }

    public function insertOptional($id, $optional, $idAuto){
        $query = "INSERT INTO `OPTIONAL_SCELTO` (IdAutoConfigurata, IdAuto, IdOptional) 
        VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sii', $id,$idAuto, $optional);
        $stmt->execute();
    }

    public function insertNewCar($modello, $prezzo, $link, $descrizione=null){
        $query = "INSERT INTO `AUTO`( `Modello`, `Prezzo_base`, `Link_immagine`, `Descrizione`) 
        VALUES (?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('siss', $modello, $prezzo, $link, $descrizione);
        $stmt->execute();
        return $this->db -> insert_id;
    }

    public function insertImmagine($modello, $descrizione, $link){
        $query = "INSERT INTO `Immagine`(`Descrizione`, `LinkImmagine`, `IdAuto`) 
        VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssi', $descrizione,$link, $modello);
        $stmt->execute();
    }

    public function updateCar($id, $modello, $prezzo, $immagine, $descrizione, $video, $audio){
        $query = "UPDATE `AUTO` SET `Modello`=?,`Prezzo_base`=?,`Link_immagine`=?,`Descrizione`=?,`Suono`=?,`Link_video`=? WHERE IdAuto=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sissssi',$modello, $prezzo,$immagine, $descrizione, $audio, $video, $id);
        return $stmt->execute();
    }

    public function insertCar($id, $venditore){
        $query = "INSERT INTO `gestisce`( `IdAuto`, `P_IVA`) 
        VALUES (?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $id,$venditore);
        $stmt->execute();
    }

    public function updateVideo($id, $video){
        $query = "UPDATE `AUTO` SET `Link_video`=? WHERE IdAuto=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$video, $id);
        $stmt->execute();
        return $stmt->error;
    }

    public function updateAudio($id, $audio){
        $query = "UPDATE `AUTO` SET `Suono`=? WHERE IdAuto=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$audio, $id);
        $stmt->execute();
        return $stmt->error;
    }

    public function getNumeroNotifiche($cf, $letto = 0){
        $query = "SELECT COUNT(*) FROM notifiche WHERE IdPrimario = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf, $letto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = 0;
        if(isset($result[0]["COUNT(*)"])){
            $num = $result[0]["COUNT(*)"];
        } else {
            $num = 0;
        }
        
        return $num;
    }

    public function insertNewMotore($idAuto,$nome, $descrizione, $prezzo, $cavalli, $cilindrata){
        $query = "INSERT INTO `MOTORE`(`IdAuto`, `Nome`, `Descrizione`, `Prezzo`, `Cilindrata`, `Cavalli`) 
        VALUES (?,?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('issiii',$idAuto,$nome, $descrizione, $prezzo, $cavalli, $cilindrata);
        $stmt->execute();
    }

    public function insertNewEsterno($idAuto, $nome, $descrizione, $immagine, $prezzo){
        $query = "INSERT INTO `ESTERNI`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`)
        VALUES (?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isssi',$idAuto,$nome, $descrizione, $immagine, $prezzo);
        $stmt->execute();
    }

    public function insertNewInterno($idAuto, $nome, $descrizione, $immagine, $prezzo){
        $query = "INSERT INTO `INTERNI`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`)
        VALUES (?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isssi',$idAuto,$nome, $descrizione, $immagine, $prezzo);
        $stmt->execute();
    }

    public function insertNewOptional($idAuto, $nome, $descrizione, $immagine, $prezzo){
        $query = "INSERT INTO `OPTIONAL`(`IdAuto`, `Nome`, `Descrizione`, `Link_Immagine`, `Prezzo`)
        VALUES (?,?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isssi',$idAuto,$nome, $descrizione, $immagine, $prezzo);
        $stmt->execute();
    }

    public function gestisce($idAuto, $venditore){
        $query = "SELECT `IdAuto`, `P_IVA` FROM `Gestisce` WHERE IdAuto=? && P_IVA=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is',$idAuto,$venditore);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
}
?>