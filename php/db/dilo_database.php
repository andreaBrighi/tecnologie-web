<?php
class DatabaseHelperDilo{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }        
    }

    public function getAste(){
        $query = "SELECT Link_Immagine1, Descrizione, Modello, prezzoAttuale FROM auto_in_asta";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomAste($n){
        $query = "SELECT Link_Immagine1, Descrizione, Modello, prezzoAttuale FROM auto_in_asta ORDER BY RAND() LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNAste($n){
        $query = "SELECT Link_Immagine1, Descrizione FROM auto_in_asta LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getFiltroCasaAste($casa){
        $query = "SELECT Link_Immagine1, Descrizione, Modello, IdAsta, prezzoAttuale FROM auto_in_asta AS A INNER JOIN venditore AS V on A.P_IVA = V.P_IVA
         WHERE V.Nome_Utente = ? LIMIT 10";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$casa);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getFiltroAvanzato($casa = NULL, $venduto = 0, $modello = NULL, $dataImm = NULL){
        $query = "SELECT Link_Immagine1, Descrizione, Modello, IdAsta, prezzoAttuale FROM auto_in_asta AS A 
        INNER JOIN venditore AS V on A.P_IVA = V.P_IVA
        WHERE V.Nome_Utente = ? AND (A.Venduto = ? OR Modello = ? OR Anno_Immatricolazione = ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('siss',$casa,$venduto,$modello,$dataImm);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAsteByIVA($casa){
        $iva = $this->getPIVA($casa);
        $query = "SELECT Modello, IdAsta FROM auto_in_asta AS A INNER JOIN venditore AS V on A.P_IVA = V.P_IVA
         WHERE V.P_IVA = ? AND Venduto = 0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$iva);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAsteChiuseByIVA($casa){
        $iva = $this->getPIVA($casa);
        $query = "SELECT Modello, IdAsta, C.Nome_Utente as Vincitore, prezzoAttuale FROM auto_in_asta AS A 
        INNER JOIN venditore AS V on A.P_IVA = V.P_IVA
         LEFT JOIN cliente AS C ON C.CF = A.Vincitore
         WHERE V.P_IVA = ? AND Venduto = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$iva);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCaseAsta(){
        $query = "SELECT P_IVA, Nome_Utente, Link_Logo FROM venditore as V
         WHERE V.Casa_Asta = true";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAsta($id){
        $query = "SELECT IdAsta, Modello, Venduto, Link_Immagine1, Link_Immagine2, Link_Immagine3, Descrizione, Prezzo_iniziale, prezzoAttuale, Link_video, Suono
        FROM auto_in_asta WHERE IdAsta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAsteVinte($cf){
        $query = "SELECT IdAsta, Modello, prezzoAttuale
        FROM auto_in_asta WHERE Vincitore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$cf);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRilanci($id){
        $query = "SELECT Importo FROM rilancio WHERE IdAsta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getPrezzoAttuale($id){
        $query = "SELECT PrezzoAttuale FROM auto_in_asta WHERE IdAsta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $prezzo = $result[0]["PrezzoAttuale"];
        return $prezzo;
    }
    
    public function chiudiAsta($idAsta){
        $query = "UPDATE auto_in_asta SET Venduto = true WHERE IdAsta = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAsta);
        $stmt->execute();

        return $stmt->error;
    }

    public function getVincitore($idAsta){
        $query = "SELECT CF FROM cliente as C 
        WHERE C.CF = (SELECT CF FROM rilancio WHERE Importo = (SELECT MAX(Importo) FROM rilancio WHERE IdAsta = ? ))";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAsta);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $cf = NULL;
        if(isset($result[0]["CF"])){
            $cf = $result[0]["CF"];
        } else {
            $cf = NULL;
        }
        
        
        return $cf;
    }

    public function setVincitore($idAsta){
        $vincitore = $this->getVincitore($idAsta);
        if($vincitore == NULL){
            return 0;
        }
        $query = "UPDATE auto_in_asta SET Vincitore = ? WHERE IdAsta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$vincitore, $idAsta);
        $stmt->execute();

        return $stmt->error;
    }

    public function getPIVA($nome){
        $query = "SELECT P_IVA FROM venditore WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nome);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $P_IVA = $result[0]["P_IVA"];
        return $P_IVA;
    }

    public function getPIVACorriere($nome){
        $query = "SELECT P_IVA FROM corriere WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nome);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $P_IVA = $result[0]["P_IVA"];
        return $P_IVA;
    }

    public function getPIVA_Corriere($nome){
        $query = "SELECT P_IVA FROM corriere WHERE Nome_Utente = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $nome);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $P_IVA = $result[0]["P_IVA"];
        return $P_IVA;
    }

    public function inserisciAutoInAsta($nome, $prezzo, $dataImm, $modello, $imm1, $imm2, $imm3, $descrizione, $suono = NULL, $video = NULL, $venduto = 0, $vincitore = NULL){
        $P_IVA = $this->getPIVA($nome);
        $query = "INSERT INTO auto_in_asta (P_IVA, Prezzo_iniziale, Anno_Immatricolazione, Modello, Link_Immagine1, Link_Immagine2, Link_Immagine3, Descrizione, prezzoAttuale, Suono, Link_video)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sissssssiss', $P_IVA, $prezzo, $dataImm, $modello, $imm1, $imm2, $imm3, $descrizione, $prezzo, $suono, $video);
        $stmt->execute();
        return $stmt->error;
    }

    public function inserisciRilancio($id, $valore, $cf){
        if($this->getPrezzoAttuale($id) >= $valore){
            return "Errore nel database";
        }
        $query = "INSERT INTO rilancio (Importo, CF, IdAsta)
        VALUES(?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('isi', $valore, $cf, $id);
        $stmt->execute();

        return $stmt->error;
    }

    public function setPrezzoAttuale($id, $valore){
        $query = "UPDATE auto_in_asta SET prezzoAttuale = ? WHERE IdAsta = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $valore, $id);
        $stmt->execute();

        return $stmt->error;
    }

    public function getPartecipanti($idAsta){
        $query = "SELECT DISTINCT C.CF, C.Email AS Email FROM cliente as C INNER JOIN rilancio as R on C.CF = R.CF
        WHERE R.IdAsta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAsta);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
/*---------------------------------------------------------------------
    public function getOrdini($cf){
        $query = "SELECT * FROM ordini as O
        INNER JOIN cliente as C on C.CF = O.CF
        INNER JOIN Auto_configurata as AC on AC.IdOrdine = O.IdOrdine
        INNER JOIN Esterni_Scelti as ES on AS.IdAutoConfigurata = AC.IdAutoConfigurata
        INNER JOIN Interni_Scelti as InS on InS.IdAutoConfigurata = AC.IdAutoConfigurata
        INNER JOIN Motore as M on M.IdAutoconfigurata = AC.IdAutoConfigurata
        INNER JOIN auto AS A on A.IdAuto = AC.IdAutoConfigurata
        WHERE C.CF = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$cf);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

/*---------------------------------------------------------------------*/

    public function getNotificheFiltrate($cf, $letto){
        $query = "SELECT Titolo, Messaggio, IdNotifica, Letto FROM notifiche WHERE IdPrimario = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf,$letto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificheFiltrateVenditore($cf, $letto){
        $query = "SELECT Titolo, Messaggio, IdNotifica, Letto FROM NOTIFICHE_VENDITORE WHERE P_IVA = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf,$letto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificheFiltrateCorriere($cf, $letto){
        $query = "SELECT Titolo, Messaggio, IdNotifica, Letto FROM NOTIFICHE_CORRIERE WHERE P_IVA = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf,$letto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotifiche($cf){
        $query = "SELECT Titolo, Messaggio, IdNotifica, Letto FROM notifiche WHERE IdPrimario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$cf);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificheVenditore($cf){
        $query = "SELECT Titolo, Messaggio, IdNotifica, Letto FROM NOTIFICHE_VENDITORE WHERE P_IVA = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$cf);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNotificheCorriere($cf){
        $query = "SELECT Titolo, Messaggio, IdNotifica, Letto FROM NOTIFICHE_CORRIERE WHERE P_IVA = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$cf);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function setNotificaLetta($idNotifica){
        $valore = 1;
        $query = "UPDATE notifiche SET Letto = ? WHERE IdNotifica = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $valore, $idNotifica);
        $stmt->execute();

        return $stmt->error;
    }

    public function setNotificaLettaVenditore($idNotifica){
        $valore = 1;
        $query = "UPDATE NOTIFICHE_VENDITORE SET Letto = ? WHERE IdNotifica = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $valore, $idNotifica);
        $stmt->execute();

        return $stmt->error;
    }

    public function setNotificaLettaCorriere($idNotifica){
        $valore = 1;
        $query = "UPDATE NOTIFICHE_CORRIERE SET Letto = ? WHERE IdNotifica = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $valore, $idNotifica);
        $stmt->execute();

        return $stmt->error;
    }

    public function inserisciNotifica($cf, $msg, $titolo){
        $query = "INSERT INTO notifiche (IdPrimario, Messaggio, Titolo)
        VALUES(?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss', $cf, $msg, $titolo);
        $stmt->execute();

        return $stmt->error;
    }

    public function getNumeroNotifiche($cf, $letto = 0){
        $query = "SELECT COUNT(*) FROM notifiche WHERE IdPrimario = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf, $letto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = 0;
        if(isset($result[0]["COUNT(*)"])){
            $num = $result[0]["COUNT(*)"];
        } else {
            $num = 0;
        }
        
        return $num;
    }

    public function getNumeroNotificheVenditore($cf, $letto = 0){
        $query = "SELECT COUNT(*) FROM NOTIFICHE_VENDITORE WHERE P_IVA = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf, $letto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = $result[0]["COUNT(*)"];
        
        return $num;
    }

    public function getNumeroNotificheCorriere($cf, $letto = 0){
        $query = "SELECT COUNT(*) FROM NOTIFICHE_CORRIERE WHERE P_IVA = ? AND Letto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si',$cf, $letto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = $result[0]["COUNT(*)"];
        
        return $num;
    }

//------------------------------------------------------------------------

    public function getOrdiniFiltrati($cf, $stato){
        $query = "SELECT O.IdOrdine, C.Nome_Utente AS Corriere, S.Nome_Stato
        FROM ordine AS O INNER JOIN stato_ordine AS S ON O.IdStato = S.IdStato
        INNER JOIN corriere AS C ON O.P_IVA = C.P_IVA
        
        WHERE O.IdStato = ? AND ? = ANY
        (SELECT auto_configurata.CF FROM auto_configurata INNER JOIN ordine ON auto_configurata.IdOrdine = ordine.IdOrdine
        WHERE auto_configurata.CF = ? AND ordine.IdOrdine = O.IdOrdine)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iss',$stato, $cf, $cf);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $lista = [];
        foreach($result as $ordine){
            $opt = $this->getAutoInOrdine($ordine["IdOrdine"]);
            $prezzoTotale = 0;
            foreach($opt as $singolaAuto){
                $prezzoTotale = $prezzoTotale + $singolaAuto["PrezzoTotale"];
            }
            $ordine["PrezzoTotale"] = $prezzoTotale;
            $ordine["autoInOrdine"] = $opt;
            array_push($lista, $ordine);
        }
        return $lista;
    }

    public function getOrdini($cf){
        //tra tutti gli ordini del cf
        //necessito di: data, totale, idordine
        //il totale è la somma di tutti i costi totali
        //auto: modello, marchio, prezzo totale, quantità
        //quantità: si fa un groupby per marchio e modello e si calcola il count
        
        $query = "SELECT O.IdOrdine, C.Nome_Utente AS Corriere, S.Nome_Stato
        FROM ordine AS O INNER JOIN stato_ordine AS S ON O.IdStato = S.IdStato
        INNER JOIN corriere AS C ON O.P_IVA = C.P_IVA
        
        WHERE ? = ANY
        (SELECT auto_configurata.CF FROM auto_configurata INNER JOIN ordine ON auto_configurata.IdOrdine = ordine.IdOrdine
        WHERE auto_configurata.CF = ? AND ordine.IdOrdine = O.IdOrdine)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$cf, $cf);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $lista = [];
        foreach($result as $ordine){
            $opt = $this->getAutoInOrdine($ordine["IdOrdine"]);
            $prezzoTotale = 0;
            foreach($opt as $singolaAuto){
                $prezzoTotale = $prezzoTotale + $singolaAuto["PrezzoTotale"];
            }
            $ordine["PrezzoTotale"] = $prezzoTotale;
            $ordine["autoInOrdine"] = $opt;
            array_push($lista, $ordine);
        }
        return $lista;
    }

    public function getAutoInOrdine($idOrdine){
        $query = "SELECT DISTINCT auto.IdAuto, AC.IdAutoConfigurata, auto.Modello, v.Nome_Utente as Marchio, AC.PrezzoTotale
        FROM auto_configurata AS ac
        INNER JOIN auto ON ac.IdAuto = auto.IdAuto
        INNER JOIN ordine AS O ON AC.IdOrdine = AC.IdOrdine
        INNER JOIN gestisce AS G ON G.IdAuto = AC.IdAuto
        INNER JOIN venditore AS V ON V.P_IVA = G.P_IVA
        WHERE AC.IdOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idOrdine);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function removeAutoDalCarrello($idAuto){
        $query = "UPDATE auto_configurata SET IdOrdine = NULL WHERE IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idAuto);
        $stmt->execute();

        return $stmt->error;
    }

    public function getAutoConfigurata($idAuto){
        $query = "SELECT A.IdAutoConfigurata, A.PrezzoTotale, AU.Modello, AU.Prezzo_base, V.Nome_Utente
        FROM auto_configurata as A
        INNER JOIN auto AS AU ON A.IdAuto = AU.IdAuto
        INNER JOIN gestisce as G ON AU.IdAuto = G.IdAuto
        INNER JOIN venditore AS V on V.P_IVA = G.P_IVA 
        where A.IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAuto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $lista = NULL;
        foreach($result as $auto){
            $opt = $this->getOptional($auto["IdAutoConfigurata"]);
            $interni = $this->getInterni($auto["IdAutoConfigurata"]);
            $motore = $this->getMotore($auto["IdAutoConfigurata"]);
            $esterni = $this->getEsterni($auto["IdAutoConfigurata"]);
            $auto["interni"] = $interni;
            $auto["esterni"] = $esterni;
            $auto["motore"] = $motore[0];
            $auto["opt"] = $opt;
            $lista = $auto;
        }
        return $lista;
    }

    public function getDettaglioOrdine($idOrdine){
        $query = "SELECT A.IdAutoConfigurata, A.PrezzoTotale, AU.Modello, AU.Prezzo_base, V.Nome_Utente
         FROM auto_configurata as A INNER JOIN ordine as Ord ON A.IdOrdine = Ord.IdOrdine 
        INNER JOIN auto AS AU ON A.IdAuto = AU.IdAuto
         INNER JOIN gestisce as G ON AU.IdAuto = G.IdAuto
          INNER JOIN venditore AS V on V.P_IVA = G.P_IVA 
            where Ord.IdOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idOrdine);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $lista = [];
        foreach($result as $auto){
            $opt = $this->getOptional($auto["IdAutoConfigurata"]);
            $interni = $this->getInterni($auto["IdAutoConfigurata"]);
            $motore = $this->getMotore($auto["IdAutoConfigurata"]);
            $esterni = $this->getEsterni($auto["IdAutoConfigurata"]);
            $auto["interni"] = $interni;
            $auto["esterni"] = $esterni;
            $auto["motore"] = $motore[0];
            $auto["opt"] = $opt;
            array_push($lista, $auto);
        }
        return $lista;
    }

    public function getAutoNelCarrello($cf){
        //prendo id delle auto configurate presenti nel carrello
        //faccio join con
        
        $query = "SELECT A.IdAutoConfigurata, A.PrezzoTotale, AU.Modello, AU.Prezzo_base, V.Nome_Utente
         FROM auto_configurata as A INNER JOIN ordine as Ord ON A.IdOrdine = Ord.IdOrdine 
        INNER JOIN auto AS AU ON A.IdAuto = AU.IdAuto
         INNER JOIN gestisce as G ON AU.IdAuto = G.IdAuto
          INNER JOIN venditore AS V on V.P_IVA = G.P_IVA 
            where A.CF = ? AND Ord.IdStato = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$cf);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $lista = [];
        foreach($result as $auto){
            $opt = $this->getOptional($auto["IdAutoConfigurata"]);
            $interni = $this->getInterni($auto["IdAutoConfigurata"]);
            $motore = $this->getMotore($auto["IdAutoConfigurata"]);
            $esterni = $this->getEsterni($auto["IdAutoConfigurata"]);
            $auto["interni"] = $interni;
            $auto["esterni"] = $esterni;
            $auto["motore"] = $motore[0];
            $auto["opt"] = $opt;
            array_push($lista, $auto);
        }
        return $lista;
    }

    public function getOptional($idAC){
        $query = "SELECT O.Nome, O.Prezzo FROM optional_scelto AS OS
        INNER JOIN auto_configurata AS A ON A.IdAutoConfigurata = OS.IdAutoConfigurata
        INNER JOIN optional as O ON O.IdOptional = OS.IdOptional
        WHERE A.IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAC);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getInterni($idAC){
        $query = "SELECT I.Nome, I.Prezzo FROM interno_scelto AS InS
        INNER JOIN auto_configurata AS A ON A.IdAutoConfigurata = InS.IdAutoConfigurata
        INNER JOIN interni as I ON I.IdInterni = InS.IdInterni
        WHERE A.IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAC);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getEsterni($idAC){
        $query = "SELECT E.Nome, E.Prezzo FROM esterni_scelte AS EnS
        INNER JOIN auto_configurata AS A ON A.IdAutoConfigurata = EnS.IdAutoConfigurata
        INNER JOIN esterni as E ON E.IdEsterni = EnS.IdEsterni
        WHERE A.IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAC);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getMotore($idAC){
        $query = "SELECT M.Nome, M.Prezzo FROM motore AS M
        INNER JOIN auto_configurata AS A ON A.IdMotore = M.IdMotore
        WHERE A.IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAC);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function creaCarrello($cf){
        $query = "INSERT INTO ordine (IdStato)
        VALUES(1)";
        $stmt = $this->db->prepare($query);
        $stmt->execute();

        return $stmt->insert_id;
    }

    public function getCarrello($cf){
        $query = "SELECT DISTINCT O.IdOrdine FROM ordine AS O
        INNER JOIN auto_configurata AS A ON O.IdOrdine = A.IdOrdine
        WHERE A.CF = ? AND O.IdStato = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $cf);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        if(isset($result[0]["IdOrdine"])){
            $id = $result[0]["IdOrdine"];
        } else {
            $id = -1;
        }        

        return $id;
    }

    public function addAutoInCarrello($idAuto, $idCarrello){
       
        $query = "UPDATE auto_configurata SET IdOrdine = ? WHERE IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $idCarrello, $idAuto);
        $stmt->execute();

        return $stmt->error;
    }

    public function setCF_Auto($idAuto, $cf){
       
        $query = "UPDATE auto_configurata SET CF = ? WHERE IdAutoConfigurata = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $cf, $idAuto);
        $stmt->execute();

        return $stmt->error;
    }

    public function getStatoOrdini(){
        $query = "SELECT * FROM stato_ordine WHERE IdStato != 1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }


//------------------------------------------------------------------------------

    public function getAuto($idAuto){
        $query = "SELECT * FROM auto WHERE IdAuto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAuto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getImmagini($idAuto){
        $query = "SELECT * FROM immagine WHERE IdAuto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAuto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getNumCommentiFiltro($idAuto, $numStelline){
        $query = "SELECT COUNT(*) FROM commento WHERE IdAuto = ? AND NumStelline = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii',$idAuto, $numStelline);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = $result[0]["COUNT(*)"];
        return $num;
    }

    public function getNumCommenti($idAuto){
        $query = "SELECT COUNT(*) FROM commento WHERE IdAuto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$idAuto);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        $num = $result[0]["COUNT(*)"];
        return $num;
    }

    public function getCommenti($idAuto, $numStelline){
        $query = "SELECT * FROM commento INNER JOIN cliente ON commento.CF = cliente.CF WHERE IdAuto = ? AND NumStelline = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ii', $idAuto, $numStelline);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);;
    }

    public function getCommento($cf, $idAuto){
        $query = "SELECT * FROM commento INNER JOIN cliente ON commento.CF = ? && idAuto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('si', $cf, $idAuto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);;
    }

    public function inserisciCommento($cf, $idAuto, $testo, $stelline){
        $query = "INSERT INTO commento (CF, IdAuto, Testo, NumStelline)
        VALUES(?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sisi', $cf, $idAuto, $testo, $stelline);
        $stmt->execute();

        return $stmt->error;
    }

}

?>