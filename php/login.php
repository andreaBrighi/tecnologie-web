<?php
require_once 'gianluca_bootstrap.php';

if(isset($_POST["username"]) && isset($_POST["password"])){
    $checkSalt = $dbh_gianluca->getSalt(str_replace(" ","",$_POST["username"]));
    if(count($checkSalt) == 0){
        $templateParams["errorelogin"] = "Errore! Controllare username!";
    } else {
        $salt = $checkSalt[0]["Salt"];
        $SaltPassword="";
        for ($i=0; $i < 128; $i++) { 
            $SaltPassword.=$salt[$i].$_POST["password"][$i];
        }
        $password = hash("sha3-512", $SaltPassword);
        $login_result = $dbh_gianluca->checkLogin(str_replace(" ","",$_POST["username"]), $password);
        if(count($login_result)==0){
            //Login fallito
            $templateParams["errorelogin"] = "Errore! Controllare username o password!";
        }
        else{
            registerLoggedUser($login_result[0]);
        }
    }
}

if(isUserLoggedIn()){
    header("location: manage_account.php");
}
else{
    $templateParams["titolo"] = "Car Shop - Login";
    $templateParams["titoloPagina"] = "Login";
    $templateParams["nome"] = "login-form.php";
    $templateParams["js"][0] = "./js/login.js";    
    $templateParams["js"][1] = "./js/SecurityPassword.js";
    $templateParams["css"][0] = "./css/gianluca_style.css";
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>