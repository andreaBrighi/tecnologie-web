<?php
require_once 'dilo_bootstrap.php';
$id = 1;

if(isset($_COOKIE["cliente"]) && isset($_SESSION["CF"])){
    //è un cliente registrato con un cookie, salvo il suo carrello
    //faccio splitting del contenuto, ad ogni auto associata al cookie setto il
    //CF e creo un carrello
    $arrayId = explode(',', $_COOKIE["cliente"]);
    //controllo l'esistenza del carrello, in caso lo creo
    $idCarrello = $dbh_dilo->getCarrello($_SESSION["CF"]);
    if($idCarrello != -1) {
        //il carrello esiste già
    } else {
        //il carrello non esiste
        $idCarrello = $dbh_dilo->creaCarrello($_SESSION["CF"]);
    }
    //Aggiungo le auto nel carrello
    foreach($arrayId as $elemento){
        $dbh_dilo->addAutoInCarrello($elemento, $idCarrello);
        $dbh_dilo->setCF_Auto($elemento, $_SESSION["CF"]);
    }
    setcookie("cliente", "", time() - 100, "/");
}
if(isset($_GET["idAuto"])){
    if(isset($_SESSION["CF"])){
        //controllo che esista un carrello per il cliente
        $idCarrello = $dbh_dilo->getCarrello($_SESSION["CF"]);
        if($idCarrello != -1) {
            //il carrello esiste già
            $dbh_dilo->addAutoInCarrello($_GET["idAuto"], $idCarrello);
        } else {
            //il carrello non esiste
            $idCarrello = $dbh_dilo->creaCarrello($_SESSION["CF"]);
            $dbh_dilo->addAutoInCarrello($_GET["idAuto"], $idCarrello);
        }
    } else {
        //l'utente non è registrato
        $idAuto = $_GET["idAuto"];
        if(!isset($_COOKIE["cliente"])){
            //il cookie non esiste: lo creo e ci inserisco la prima auto
            setcookie("cliente", $idAuto, time() + (60 * 60 * 24 * 30), "/");
        } else {
            //il cookie esiste e ci aggiungo l'auto
            $testo = $_COOKIE["cliente"].",".$idAuto;
            setcookie("cliente", $testo, time() + (60 * 60 * 24 * 30), "/");
        }
    }
}

if(isset($_GET["Elimina"])){
    $dbh_dilo->removeAutoDalCarrello($_GET["Elimina"]);
}

if(isset($_GET["tipologia"])){
    if($_GET["tipologia"] == "carrello"){
        if(isset($_SESSION["CF"])){
            $auto = $dbh_dilo->getAutoNelCarrello($_SESSION["CF"]);
            $templateParams["auto"] = $auto;
            if(count($auto) == 0){
                $templateParams["carrelloVuoto"] = 1;
            }
            $templateParams["idCarrello"] = $dbh_dilo->getCarrello($_SESSION["CF"]);
        } else {
            if(isset($_COOKIE["cliente"])){
                $auto = [];
                $arrayId = explode(',', $_COOKIE["cliente"]);
                foreach($arrayId as $elemento){
                    $autoConfigurata = $dbh_dilo->getAutoConfigurata($elemento);
                    array_push($auto, $autoConfigurata);
                }
                $templateParams["auto"] = $auto;
            } else {
                $templateParams["carrelloVuoto"] = 1;
            }
        }
        $templateParams["carrello"] = 1;
        $templateParams["titolo"] = "Car Shop - Carrello";
        $templateParams["titoloPagina"] = "Carrello";
        $templateParams["nome"] = "template/carrello.php";
    } else {
        if($_GET["tipologia"] == "dettaglio"){
            if(isset($_GET["idOrdine"])){
                $templateParams["auto"] = $dbh_dilo->getDettaglioOrdine((int)$_GET["idOrdine"]);
            }
            $templateParams["titolo"] = "CarShop - Dettaglio";
            $templateParams["titoloPagina"] = "Dettaglio";
            $templateParams["nome"] = "template/carrello.php";
        } else {
            if(isset($_SESSION["CF"])){
                if(isset($_GET["filtra"])){
                    $templateParams["ordini"] = $dbh_dilo->getOrdiniFiltrati($_SESSION["CF"], $_GET["filtra"]);
                } else {
                    $templateParams["ordini"] = $dbh_dilo->getOrdini($_SESSION["CF"]);
                }
                $templateParams["stato"] = $dbh_dilo->getStatoOrdini();
            } else {
                $templateParams["msg"] = "Non hai effettuato l'accesso";
            }
            $templateParams["titolo"] = "CarShop - Ordini";
            $templateParams["titoloPagina"] = "Ordini";
            $templateParams["nome"] = "template/ordini.php";
        }
        
    }
    
} else {
    header("Location: gestione_carrello.php?tipologia=carrello");
}
if(isset($_SESSION["CF"])){
    $numNotificheNonLette = $dbh_dilo->getNumeroNotifiche($_SESSION["CF"]);
    $templateParams["numNotifiche"] = $numNotificheNonLette;
}
$templateParams["css"] = ["css/Dilo_style.css"];
require 'template/struttura.php';
?>