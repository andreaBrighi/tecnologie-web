<?php
require_once 'gianluca_bootstrap.php';

if(isUserLoggedIn() && $_SESSION["CF"] != ""){
	$templateParams["titolo"] = "Car Shop - Desideri";
	$templateParams["titoloPagina"] = "Desideri";
	$templateParams["nome"] = "Ultimi_desideri.php";
	$templateParams["ultimi_desideri"] = $dbh_gianluca->getLastWish($_SESSION["CF"], 6);
	if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE"){
		$templateParams["numNotifiche"] = $dbh_gianluca->getNumeroNotifiche($_SESSION["CF"]);
	}
	$templateParams["css"][0] = "./css/gianluca_style.css";
}
else{
    header("location: login.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>