<?php
function isActive($pagename){
    if(basename($_SERVER['PHP_SELF'])==$pagename){
        echo " class='active' ";
    }
}

function isUserLoggedIn(){
    return !empty($_SESSION['Nome_Utente']);
}

function registerLoggedUser($user){
    $_SESSION["Nome_Utente"] = $user["Nome_Utente"];
    $_SESSION["tipo"] = $user["tipo"];
    $_SESSION["CF"] = $user["CF"];
    if($_SESSION["tipo"] == "VENDITORE"){
        $_SESSION["Casa_Asta"] = $user["Casa_Asta"];
    }
}

function registerTemporaryUserChangePass($user){
    $_SESSION["Nome_Utente"] = $user["Nome_Utente"];
    $_SESSION["tipo"] = $user["tipo"];
    $_SESSION["CF"] = "";
    $_SESSION["Ripristino"] = "1";
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $imageName = str_replace(" ", "_", $imageName);
    $fullPath = $path.$imageName;
    
    $maxKB = 500;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    $msg = str_replace(" ", "_", $msg);
    return array($result, $msg);
}

function sendEmail($destinatario, $messaggio, $oggetto){
    mail($destinatario, $oggetto, $messaggio);
}

function uploadSound($path, $sound){
    $soundName = basename($sound["name"]);
    $soundName = str_replace(" ", "_", $soundName);
    $fullPath = $path.$soundName;

    $maxKB = 5000;
    $acceptedExtensions = array("mp3", "wav");
    $result = 0;
    $msg = "";

    //Controllo se immagine è veramente un'immagine
    $soundSize = filesize($sound["tmp_name"]);
    if($soundSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }

    //Controllo estensione del file
    $soundFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($soundFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $soundName = pathinfo(basename($sound["name"]), PATHINFO_FILENAME)."_$i.".$soundFileType;
        }
        while(file_exists($path.$soundName));
        $fullPath = $path.$soundName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($sound["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $soundName;
        }
    }
    $msg = str_replace(" ", "_", $msg);
    return array($result, $msg);
    
}

function uploadVideo($path, $video){
    $videoName = basename($video["name"]);
    $videoName = str_replace(" ", "_", $videoName);
    $fullPath = $path.$videoName;

    $maxKB = 50000;
    $acceptedExtensions = array("mp4", "mov");
    $result = 0;
    $msg = "";

    //Controllo se immagine è veramente un'immagine
    $videoSize = filesize($video["tmp_name"]);
    if($videoSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }

    //Controllo estensione del file
    $videoFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($videoFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $videoName = pathinfo(basename($video["name"]), PATHINFO_FILENAME)."_$i.".$videoFileType;
        }
        while(file_exists($path.$videoName));
        $fullPath = $path.$videoName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($video["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $videoName;
        }
    }
    $msg = str_replace(" ", "_", $msg);
    return array($result, $msg);
    
}
?>