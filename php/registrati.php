<?php
require_once 'gianluca_bootstrap.php';

if(isset($_POST["email"]) && isset($_POST["tel"]) && isset($_POST["nomeutente"]) && isset($_POST["password"]) && isset($_POST["Conf_password"]) && $_POST["password"] === $_POST["Conf_password"]){
	//ALLORA POSSO REGISTRARMI! perchè sono obbligatori per tutti!
	$msg="";
	$salt = hash('sha3-512', uniqid(mt_rand(1, mt_getrandmax()), true));
	$SaltPassword="";
	for ($i=0; $i < 128; $i++) { 
		$SaltPassword.=$salt[$i].$_POST["password"][$i];
	}
	$password = hash("sha3-512", $SaltPassword);
	switch ($_POST["options_utente"]) {
		case 'Cliente':
			if(isset($_POST["nome"]) && isset($_POST["cognome"]) && isset($_POST["cf"]) && strlen($_POST["cf"]) == 16){

				$err = $dbh_gianluca->insertCliente($salt, $_POST["cf"], $_POST["nome"], $_POST["cognome"], $_POST["email"], $_POST["tel"], str_replace(" ","",$_POST["nomeutente"]), $password);
		        if($err==""){
		            header("location: login.php");
		        }
		        else{
		            $msg = "Errore in inserimento Cliente!";
		        }
			} else {
				$msg = "inserisci tutti i parametri corretti!";
			}			
			break;
		case 'Venditore':
			if(isset($_POST["P_IVA"]) && isset($_POST["casa_asta"]) && isset($_FILES["Logo_Azienda"]) && isset($_FILES["ImgCopertina_Azienda"]) && strlen($_POST["P_IVA"]) == 11){
				list($logo_result, $msg_logo) = uploadImage(UPLOAD_DIR, $_FILES["Logo_Azienda"]);
				list($copertina_result, $msg_copertina) = uploadImage(UPLOAD_DIR, $_FILES["ImgCopertina_Azienda"]);
				if($logo_result != 0 && $copertina_result != 0){
					$err = $dbh_gianluca->insertVenditore($salt, $_POST["P_IVA"], $_POST["casa_asta"], $_POST["email"], $_POST["tel"], str_replace(" ","",$_POST["nomeutente"]), $password, $msg_logo, $msg_copertina);
			        if($err==""){
			            header("location: login.php");
			        }
			        else{
			            $msg = "Errore in inserimento Venditore!";
			        }
				} else {
					$msg = "ERRORE! ".$msg_logo." - ".$msg_copertina;
				}
			} else {
				$msg = "inserisci tutti i parametri corretti!";
			}
			break;
		case 'Corriere':
			if(isset($_POST["P_IVA"]) && strlen($_POST["P_IVA"]) == 11){
				$err = $dbh_gianluca->insertCorriere($salt, $_POST["P_IVA"], $_POST["email"], $_POST["tel"], str_replace(" ","",$_POST["nomeutente"]), $password);
		        if($err==""){
		            header("location: login.php");
		        }
		        else{
		            $msg = "Errore in inserimento Corriere!";
		        }
		    } else {
		    	$msg = "inserisci tutti i parametri corretti!";
		    }
			break;
		
		default:
			#ERRORE
			$msg = "Errore tipologia sconosciuta! Contattare amministratore!";
			break;
	}
	$templateParams["erroreRegistrazione"] = $msg;
}

if(isUserLoggedIn()){
    header("location: manage_account.php");
}
else{
    $templateParams["titolo"] = "Car Shop - Registrazione";
	$templateParams["titoloPagina"] = "Registrazione";
	$templateParams["nome"] = "registrazione-form.php";
	$templateParams["js"][0] = "./js/registrazioni.js";
	$templateParams["js"][1] = "./js/SecurityPassword.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>