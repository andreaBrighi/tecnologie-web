<?php
require_once 'gianluca_bootstrap.php';

if(isUserLoggedIn()){
	if(isset($_GET["LogOut"])){
		$_SESSION = array();
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		session_destroy();
		header('Location: login.php');
	}

	if($_SESSION["tipo"] != "CORRIERE" && isset($_POST["via"]) && isset($_POST["N_Civico"]) && isset($_POST["Citta"]) && isset($_POST["Provincia"]) && isset($_POST["CAP"])){
		$result_modifica = $dbh_gianluca->updateIndirizzo($_SESSION["tipo"], $_SESSION["Nome_Utente"], $_POST["via"], $_POST["N_Civico"], $_POST["Citta"], $_POST["Provincia"], $_POST["CAP"]);
		if($result_modifica){
			$templateParams["erroreIndirizzo"] = "Aggiornamento effettuato";
		} else {
			$templateParams["erroreIndirizzo"] = "Errore! Controllare dati inseriti";
		}
	}

	if($_SESSION["tipo"] == "VENDITORE" && isset($_POST["aggiorna"]) && $_POST["aggiorna"] == "Aggiorna foto" && isset($_FILES["Logo_Azienda"]) && isset($_FILES["ImgCopertina_Azienda"])){
		$templateParams["erroreSendImages"] = "";
		if($_FILES["Logo_Azienda"]["name"] != "" || $_FILES["ImgCopertina_Azienda"]["name"] != ""){
			if($_FILES["Logo_Azienda"]["name"] != ""){
				list($logo_result, $msg_logo) = uploadImage(UPLOAD_DIR, $_FILES["Logo_Azienda"]);
				$result_modifica_logo = $dbh_gianluca->updateLogo($_SESSION["Nome_Utente"], $msg_logo);
				if($result_modifica_logo){
					$templateParams["erroreSendImages"] = "Aggionato Logo ";
				} else {
					$templateParams["erroreSendImages"] = "Errore! Controllare aggiornamento Logo ";
				}
			} 
			if($_FILES["ImgCopertina_Azienda"]["name"] != ""){
				list($copertina_result, $msg_copertina) = uploadImage(UPLOAD_DIR, $_FILES["ImgCopertina_Azienda"]);
				$result_modifica_copertina = $dbh_gianluca->updateCopertina($_SESSION["Nome_Utente"], $msg_copertina);
				if($result_modifica_copertina){
					$templateParams["erroreSendImages"] .= "Aggionata Copertina";
				} else {
					$templateParams["erroreSendImages"] .= "Errore! Controllare aggiornamento Copertina";
				}
			} 
		} else {
			$templateParams["erroreSendImages"] = "Errore Aggiornamenti!";
		}	
	}	

	if($_SESSION["tipo"] == "CORRIERE" && isset($_POST["OrdineAssociato"]) && isset($_POST["StatoOrdine"])){
		//Aggiorna lo stato dell'ordine!
		$Ordine = explode(";", $_POST["OrdineAssociato"]);
		if(count($Ordine) == 2){
			if($Ordine[1] == $_POST["StatoOrdine"]){
				$templateParams["erroreUpdateOrdini"] = "Errore! Selezionare almeno lo stato successivo a quello attuale!";
			} else {
				$result_modifica_Ordine = $dbh_gianluca->updateStatusOrdine($Ordine[0], $_POST["StatoOrdine"]);
				if($result_modifica_Ordine){
					if($_POST["StatoOrdine"] == 3){
						//INFORMO VENDITORE
						$venditoreAssociati = $dbh_gianluca->getNomeUtentePIVAEmailVenditoreFromIdOrdine($Ordine[0]);
						if(count($venditoreAssociati) != 0){
							foreach ($venditoreAssociati as $venditoreassociato) {
								$Messaggio = getMessageFromStatusForVenditore($_POST["StatoOrdine"], $venditoreassociato["Nome_Utente"], $Ordine[0]);
								$err_notificaVenditore = $dbh_gianluca->insertNotificaVenditore($Ordine[0], $venditoreassociato["P_IVA"], $Messaggio, "Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore");
								sendEmail($venditoreassociato["Email"], $Messaggio, "Auto ordinata e pagata, il corriere passerà a ritirarla - Venditore");
							}
				        }
					}

					if($_POST["StatoOrdine"] == 6){
						//INFORMO CORRIERE
						$corriereAssociato = $dbh_gianluca->getNomeUtentePIVAEmailCorriereFromIdOrdine($Ordine[0]);
						if($corriereAssociato != NULL){
							$Messaggio = getMessageFromStatusForCORRIERE($_POST["StatoOrdine"], $corriereAssociato["Nome_Utente"], $Ordine[0]);
							$err_notificaCorriere = $dbh_gianluca->insertNotificaCorriere($Ordine[0], $corriereAssociato["P_IVA"], $Messaggio, "Conferma consegna ordine effettuato - Corriere");
							sendEmail($corriereAssociato["Email"], $Messaggio, "Conferma consegna ordine effettuato - Corriere");
						}
						//INFORMO VENDITORI
						$venditoreAssociati = $dbh_gianluca->getNomeUtentePIVAEmailVenditoreFromIdOrdine($Ordine[0]);
						if(count($venditoreAssociati) != 0){
							foreach ($venditoreAssociati as $venditoreassociato) {
								$Messaggio = getMessageFromStatusForVenditore($_POST["StatoOrdine"], $venditoreassociato["Nome_Utente"], $Ordine[0]);
								$err_notificaVenditore = $dbh_gianluca->insertNotificaVenditore($Ordine[0], $venditoreassociato["P_IVA"], $Messaggio, "Conferma consegna ordine ricevuto - Venditore");
								sendEmail($venditoreassociato["Email"], $Messaggio, "Conferma consegna ordine ricevuto - Venditore");
							}
				        }
					}
					$CFNOME = $dbh_gianluca->getCFNomeUtenteFromOrdine($Ordine[0])[0];
					$Messaggio = getMessageFromStatus($_POST["StatoOrdine"], $CFNOME["Nome_Utente"], $Ordine[0]);
					$err_notifica = $dbh_gianluca->insertNotifica($Ordine[0], $CFNOME["CF"], $Messaggio, "Aggiornamento stato Ordine - Cliente");

					if($err_notifica == ""){
						sendEmail($dbh_gianluca->getEmailFromClienteCF($CFNOME["CF"])[0]["Email"], $Messaggio, "Aggiornamento stato Ordine - Cliente");
						$templateParams["erroreUpdateOrdini"] = "Aggiornamento effettuato correttamente!";
					} else {
						$templateParams["erroreUpdateOrdini"] = "Aggiornamento effettuato, ma niente notifica per cliente";
					}
				} else {
					$templateParams["erroreUpdateOrdini"] = "Errore! Controllare dati inseriti";
				}
			}
		} else if($Ordine[0] == "Nessuno") {
			$templateParams["erroreUpdateOrdini"] = "Errore! Seleziona un ordine!";
		} else {
			$templateParams["erroreUpdateOrdini"] = "Errore! Problema inserimenti informazioni in Combobox";
		}
	}

	$templateParams["titolo"] = "Car Shop - Gestione account";
	$templateParams["titoloPagina"] = "Gestione account";
	$templateParams["nome"] = "gestione_Account.php";
	$templateParams["js"][0] = "./js/manage_account.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";
	if($_SESSION["tipo"] == "CLIENTE" || $_SESSION["tipo"] == "VENDITORE") {
		$templateParams["Indirizzo"] = $dbh_gianluca->getMyIndirizzo($_SESSION["tipo"], $_SESSION["Nome_Utente"])[0];
	} elseif ($_SESSION["tipo"] == "CORRIERE") {
		$templateParams["ordini"] = $dbh_gianluca->getMyOrdiniCorriere($_SESSION["Nome_Utente"]);
		$templateParams["statoOrdini"] = $dbh_gianluca->getAllStatiOrdine();
	}

	if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE"){
		$templateParams["numNotifiche"] = $dbh_gianluca->getNumeroNotifiche($_SESSION["CF"]);
	}
}
else{
    header("location: login.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>