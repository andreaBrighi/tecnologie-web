<?php
require_once 'gianluca_bootstrap.php';

if(isUserLoggedIn() && $_SESSION["tipo"] == "CLIENTE" && isset($_GET["IdOrdine"])){
	$templateParams["infoFatturazione"] = false;

	$PtotOrdine = $dbh_gianluca->getPrezzoTotaleOrdine($_GET["IdOrdine"], $_SESSION["CF"]);
	if($PtotOrdine[0]["PrezzoTotale"] == 0){
		header("location: index.php");
	} else {
		$templateParams["infoPrezzoTotale"] = $PtotOrdine[0]["PrezzoTotale"];
	}
	$indirizzo = $dbh_gianluca->getMyIndirizzo("CLIENTE", $_SESSION["Nome_Utente"]);
	if(count($indirizzo) != 0 && $indirizzo[0]["Via"] != NULL){
		$templateParams["infoFatturazione"] = true;
	}
	$templateParams["infoSpedizione"] = $dbh_gianluca->getMySpedizioni($_SESSION["CF"]);
	$templateParams["corrieri"] = $dbh_gianluca->getAllCorrieri();

	if(isset($_POST["spedizione"]) && isset($_POST["fatturazione"]) && isset($_POST["corriere"])){
		//EFFETTUA PAGAMENTO!
		if($_POST["spedizione"] == "INSERIRE INDIRIZZO"){
			$templateParams["errorePagamento"] = "Prima di pagare devi creare il tuo indirizzo dalla gestione account!";
		} else {
			$controlloPagamento = $dbh_gianluca->checkOrdineGiaPagato($_GET["IdOrdine"]);
			if($controlloPagamento[0]["IdPagamento"] == NULL){
				$IdMetodoPagamento = $dbh_gianluca->getIdMetodoPagamento($_SESSION["CF"], $_POST["fatturazione"]);
				if(count($IdMetodoPagamento) == 0){
					$errInserimento = $dbh_gianluca->insertMetodoPagamento($_SESSION["CF"], $_POST["fatturazione"]);
					if($errInserimento == ""){
						$IdMetodoPagamento = $dbh_gianluca->getIdMetodoPagamento($_SESSION["CF"], $_POST["fatturazione"]);
			        }
			        else{
			            $templateParams["errorePagamento"] = "Problema Inserimento Metodo Pagamento";
			        }				
				}
				if(!isset($errInserimento) || (isset($errInserimento) && $errInserimento == "")){
					$IdMetodoPagamento = $IdMetodoPagamento[0]["IdMetodoPagamento"];
					$IdPagamento = $dbh_gianluca->insertAndgetIdPagamento($IdMetodoPagamento);
					if(count($IdPagamento) != 0){
						$err_update = $dbh_gianluca->updateStatoOrdineToRichiesta($_GET["IdOrdine"], $IdPagamento[0]["IdPagamento"], $_POST["spedizione"], $_POST["corriere"]);
						if($err_update){
							$corriereAssociato = $dbh_gianluca->getNomeUtentePIVAEmailCorriereFromIdOrdine($_GET["IdOrdine"]);
							if($corriereAssociato != NULL){
								$Messaggio = getMessageFromStatusForCORRIERE(2, $corriereAssociato["Nome_Utente"], $_GET["IdOrdine"]);
								$err_notificaCorriere = $dbh_gianluca->insertNotificaCorriere($_GET["IdOrdine"], $corriereAssociato["P_IVA"], $Messaggio, "Aggiunta ordine da ritirare e consegnare - Corriere");
								sendEmail($corriereAssociato["Email"], $Messaggio, "Aggiunta ordine da ritirare e consegnare - Corriere");
							}

							$Messaggio = "Buongiorno ".$_SESSION["Nome_Utente"].", il tuo pagamento di ".$templateParams["infoPrezzoTotale"]." euro per l'ordine ".$_GET["IdOrdine"]." è andato a buon fine, nei prossimi giorni riceverà notifiche dal corriere riguardo la spedizione. Grazie.";
							$err_notifica = $dbh_gianluca->insertNotifica($_GET["IdOrdine"], $_SESSION["CF"], $Messaggio, "Aggiornamento stato Ordine Pagamento - Cliente");

							if($err_notifica == ""){
								sendEmail($dbh_gianluca->getEmailFromClienteCF($_SESSION["CF"])[0]["Email"], $Messaggio, "Aggiornamento stato Ordine Pagamento - Cliente");
								$templateParams["errorePagamento"] = "Pagamento effettuato Correttamente!";
							} else {
								$templateParams["errorePagamento"] = "Pagamento effettuato! Problema notifica!";
							}
				        }
				        else{
				            $templateParams["errorePagamento"] = "Aggiornamento Ordine non a buon fine!";
				        }
					} else {
						$templateParams["errorePagamento"] = "Problema creazioen IdPagamento";
					}				
				}
			} else {
				$templateParams["errorePagamento"] = "Ordine già Pagato!";
			}		
		}
	}

	$templateParams["titolo"] = "Car Shop - Pagamento";
	$templateParams["titoloPagina"] = "Pagamento";
	$templateParams["nome"] = "gestione_Pagamento.php";
	$templateParams["js"][0] = "./js/pagamento.js";
	$templateParams["css"][0] = "./css/gianluca_style.css";

	if(isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "CLIENTE"){
		$templateParams["numNotifiche"] = $dbh_gianluca->getNumeroNotifiche($_SESSION["CF"]);
	}
}
else{
    header("location: login.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>