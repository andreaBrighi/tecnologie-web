<?php
require_once 'gianluca_bootstrap.php';

if(isUserLoggedIn() && isset($_SESSION["tipo"]) && $_SESSION["tipo"] == "VENDITORE" && $_SESSION["Casa_Asta"] === 0){
    $templateParams["titolo"] = "Car Shop - mia concessionaria";
    $templateParams["titoloPagina"] = "La mia concessionaria";
    $templateParams["nome"] = "concessionaria.php";
    $templateParams["mieAuto"] = $dbh_gianluca->getAllMyCar($_SESSION["Nome_Utente"]);
    $templateParams["css"][0] = "./css/concessionaria.css";
}
else{
    header("location: index.php");
}

//require 'template/base_____.php';
require 'template/struttura.php';
?>