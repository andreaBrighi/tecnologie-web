<div class="mr-1 ml-1 mx-auto" id="containerOrdini">

    <?php if(isset($templateParams["msg"])): ?>
        <div class="alert alert-danger" role="alert">
            <?php echo $templateParams["msg"]; ?>
            <a class="btn btn-primary" href="login.php"> Accedi </a>
        </div>
    <?php endif; ?>

    <?php if(isset($templateParams["stato"])): ?>
    <form method="GET" action="gestione_carrello.php">
        <div class="container-fluid">
            <div class="row align-items-center text-center">
                <div class="col col-sm-6">
                    <label for="filtra"> Selezionala tipologia di ordine da visualizzare </label>
                    <input type="hidden" value="ordini" name="tipologia"/>
                    <select class="form-control form-control-sm" id="filtra" name="filtra">
                        <?php foreach($templateParams["stato"] as $stato): ?>
                        <option value="<?php echo $stato["IdStato"]; ?>"><?php echo $stato["Nome_Stato"]; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col col-sm-6">
                    <input type="submit" class="btn btn-primary mb-2" value="Filtra"/>
                    <a href="gestione_carrello.php?tipologia=ordini" class="mb-2 btn btn-primary"> Visualizza tutti gli ordini </a>
                </div>
            </div>
        </div>
    </form>
    <?php endif;?>

    <?php 
    if(isset($templateParams["ordini"])):
        foreach($templateParams["ordini"] as $ordine):
    ?>
    <div class="container-fluid p-0 pb-2 mt-2 mb-2 overflow-hidden text-center" style="border: 2px solid black; border-radius: 5px; ">
        <h2> Ordine <?php echo $ordine["IdOrdine"]; ?> </h2>
        <div class="row m-2">
            <div class="col text-center">
                <p> Stato: <?php echo $ordine["Nome_Stato"]; ?></p>
                <p> Corriere: <?php echo $ordine["Corriere"]; ?></p>
                <p> Totale: <?php echo $ordine["PrezzoTotale"]; ?> €</p>
            </div>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Marchio</th>
                <th scope="col">Modello</th>
                <th scope="col">Prezzo</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($ordine["autoInOrdine"] as $auto): ?>
            <tr>
                <td><?php echo $auto["Marchio"]; ?></td>
                <td><?php echo $auto["Modello"]; ?></td>
                <td><?php echo $auto["PrezzoTotale"]; ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <form method="GET" action="gestione_carrello.php">
            <input type="hidden" value="dettaglio" name="tipologia"/>
            <input type="hidden" value="<?php echo $ordine["IdOrdine"]; ?>" name="idOrdine"/>
            <button type="submit" aria-label="Vai al dettaglio dell'ordine" class="btn btn-primary"> Dettagli </button>
        </form>

    </div>
    <?php endforeach;
    endif; ?>

</div>