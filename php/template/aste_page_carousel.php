<h2 class="text-center"> Immagini </h2>
<div id="caroselloAsta" class="carousel slide mt-3 mx-auto" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#caroselloAsta" data-slide-to="0" class="active barreCarosello"></li>
      <li data-target="#caroselloAsta" data-slide-to="1" class="barreCarosello"></li>
      <li data-target="#caroselloAsta" data-slide-to="2" class="barreCarosello"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
        <img class="d-block w-100 caroselloAstaImmagine" aria-hidden="false" src="upload/<?php echo $templateParams["infoAsta"][0]["Link_Immagine1"]; ?>" alt="Prima slide">
        </div>
        <div class="carousel-item">
        <img class="d-block w-100 caroselloAstaImmagine" aria-hidden="false" src="upload/<?php echo $templateParams["infoAsta"][0]["Link_Immagine2"]; ?>" alt="Seconda slide">
        </div>
        <div class="carousel-item">
        <img class="d-block w-100 caroselloAstaImmagine" aria-hidden="false" src="upload/<?php echo $templateParams["infoAsta"][0]["Link_Immagine3"]; ?>" alt="Terza slide">
        </div>
    </div>
    <a class="carousel-control-prev noOpacity" href="#caroselloAsta" role="button" data-slide="prev">
    <em class="fas fa-arrow-alt-circle-left iconCarousel" aria-hidden="true"></em>
      <span class="sr-only">Immagine precedente</span>
    </a>
    <a class="carousel-control-next noOpacity" href="#caroselloAsta" role="button" data-slide="next">
    <em class="fas fa-arrow-alt-circle-right iconCarousel" aria-hidden="true"></em>
      <span class="sr-only">Immagine successiva</span>
    </a>
  </div>