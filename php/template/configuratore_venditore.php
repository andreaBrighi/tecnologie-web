<div>
    <?php if(!isset($templateParams["auto"])):?>
        <header>
            <div>
                <img src="<?=UPLOAD_DIR.$templateParams["marca"]["Link_logo"]?>" alt="<?=$templateParams["marca"]["Nome_Utente"]?>"/>   
            </div>
            <p><?=$templateParams["marca"]["Nome_Utente"]?></p>
        </header>
        <div>
            <h1>Inserisci nuovo modello</h1>
            <form action="configuratore_venditore.php" method="POST" id="auto" enctype="multipart/form-data">
                <input type="hidden" value="auto" name="tipo">
                <input type="hidden" value="<?=$templateParams["marca"]["P_IVA"] ?>" name="venditore">
                <ul>
                    <li><label for="modello">Modello</label></li>
                    <li><input type="text" id="modello" name="modello" required/></li>
                    <?php if(isset($templateParams["erroreModello"])): ?>
                    <li><p><?=$templateParams["erroreModello"]?></p></li>
                    <?php endif;?>
                    <li><label for="descrizione">Descrizione</label></li>
                    <li><textarea id="descrizione" name="descrizione"></textarea></li>
                    <li><label for="prezzo">Prezzo base</label></li>
                    <li><input type="number" id="prezzo" name="prezzo" min="4000" max="10000000" value="200000" required/></li>
                    <?php if(isset($templateParams["errorePrezzo"])): ?>
                    <li><p><?=$templateParams["errorePrezzo"]?></p></li>
                    <?php endif;?>
                    <li><label for="copertina">Aggiorna l'immagine di copertina:</label></li>
                    <li><input type="file" id="copertina" name="copertina" accept="image/*" required></li>
                    <?php if(isset($templateParams["erroreSendImage"])): ?>
                    <li><p><?=$templateParams["erroreSendImage"]?></p></li>
                    <?php endif;?>
                    <li><label for="video">Aggiorna il video dell'auto:</label></li>
                    <li><input type="file" id="video" name="video" accept="video/*"></li>
                    <?php if(isset($templateParams["erroreSendVideo"])): ?>
                    <li><p><?=$templateParams["erroreSendVideo"]?></p></li>
                    <?php endif;?>
                    <li><label for="suono">Aggiorna il suono del motore:</label></li>
                    <li><input type="file" id="suono" name="suono" accept="audio/*"></li>
                    <?php if(isset($templateParams["erroreSendAudio"])): ?>
                    <li><p><?=$templateParams["erroreSendAudio"]?></p></li>
                    <?php endif;?>
                    <li><input type="submit" value="aggiungi nuovo modello"/></li>
                </ul>
            </form>
        </div>
    <?php else:?>
        <header>
            <div>
                <img src="<?=UPLOAD_DIR.$templateParams["auto"]["Link_immagine"]?>" alt="<?=$templateParams["auto"]["Modello"]?>"/>
            </div>
        </header>
        <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link 
                <?php if(!isset($templateParams["actual"]) || (isset($templateParams["actual"]) && $templateParams["actual"]=="update")):?>
                    active
                    <?php endif;?>
                    " id="pills-update-tab" data-toggle="pill" href="#pills-update" role="tab"
                    aria-controls="pills-update" aria-selected="true">Update</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="motore")):?>
                    active
                    <?php endif;?>" id="pills-motore-tab" data-toggle="pill" href="#pills-motore" role="tab"
                    aria-controls="pills-motore" aria-selected="false">Motore</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="esterni")):?>
                    active
                    <?php endif;?>" id="pills-esterni-tab" data-toggle="pill" href="#pills-esterni" role="tab"
                    aria-controls="pills-esterni" aria-selected="false">Esterni</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="interni")):?>
                    active
                    <?php endif;?>" id="pills-interni-tab" data-toggle="pill" href="#pills-interni" role="tab"
                    aria-controls="pills-interni" aria-selected="false">Interni</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="optional")):?>
                    active
                    <?php endif;?>" id="pills-optional-tab" data-toggle="pill" href="#pills-optional" role="tab"
                    aria-controls="pills-optional" aria-selected="false">Optional</a>
            </li>
        </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade <?php if(!isset($templateParams["actual"]) || (isset($templateParams["actual"]) && $templateParams["actual"]=="update")):?>
                     show active
                    <?php endif;?>" id="pills-update" role="tabpanel" aria-labelledby="pills-update-tab">
            <div>
                <h3>Aggiorna dati</h3>
                <form action="configuratore_venditore.php" method="POST" id="update" enctype="multipart/form-data">
                    <input type="hidden" value="update" name="tipo">
                    <input type="hidden" value="<?=$templateParams["auto"]["IdAuto"]?>" name="auto">
                    <ul>
                        <li><label for="modello">Modello</label></li>
                        <li><input type="text" id="modello" name="modello" value="<?=$templateParams["auto"]["Modello"]?>"/></li>
                        <?php if(isset($templateParams["erroreModello"])): ?>
                        <li><p><?=$templateParams["erroreModello"]?></p></li>
                        <?php endif;?>
                        <li><label for="descrizione">Descrizione</label></li>
                        <li><textarea id="descrizione" name="descrizione"><?=$templateParams["auto"]["Descrizione"]?></textarea></li>
                        <li><label for="prezzo">Prezzo base</label></li>
                        <li><input type="number" id="prezzo" name="prezzo" min="4000" max="10000000" value="<?=$templateParams["auto"]["Prezzo_base"]?>"/></li>
                        <?php if(isset($templateParams["errorePrezzo"])): ?>
                        <li><p><?=$templateParams["errorePrezzo"]?></p></li>
                        <?php endif;?>
                        <li><label for="copertina">Aggiorna l'immagine di copertina:</label></li>
                        <li><input type="file" id="copertina" name="copertina" accept="image/*"></li>
                        <?php if(isset($templateParams["erroreSendImage"])): ?>
                        <li><p><?=$templateParams["erroreSendImage"]?></p></li>
                        <?php endif;?>
                        <li><label for="video">Aggiorna il video dell'auto:</label></li>
                        <li><input type="file" id="video" name="video" accept="video/*"></li>
                        <?php if(isset($templateParams["erroreSendImage"])): ?>
                        <li><p><?=$templateParams["erroreSendImage"]?></p></li>
                        <?php endif;?>
                        <li><label for="suono">Aggiorna il suono del motore:</label></li>
                        <li><input type="file" id="suono" name="suono" accept="audio/*"></li>
                        <?php if(isset($templateParams["erroreSendAudio"])): ?>
                        <li><p><?=$templateParams["erroreSendAudio"]?></p></li>
                        <?php endif;?>
                        <li><input type="submit" value="aggiorna"/></li>
                    </ul>
                </form>
            </div>
            <div>
                <h3>Inserisci nuova foto</h3>
                <form action="configuratore_venditore.php" method="POST" id="immagini" enctype="multipart/form-data">
                    <input type="hidden" value="immagini" name="tipo">
                    <input type="hidden" value="<?=$templateParams["auto"]["IdAuto"]?>" name="modello">
                    <ul>
                        <li><label for="im-descrizione">Descrizione</label></li>
                        <li><textarea id="im-descrizione" name="descrizione" required></textarea></li>
                        <?php if(isset($templateParams["erroreImDescrizione"])): ?>
                        <li><p><?=$templateParams["erroreImDescrizione"]?></p></li>
                        <?php endif;?>
                        <li><label for="im-immagine">Inserisci immagine:</label></li>
                        <li><input type="file" id="im-immagine" name="immagine" accept="image/*" required></li>
                        <?php if(isset($templateParams["erroreImImage"])): ?>
                        <li><p><?=$templateParams["erroreImImage"]?></p></li>
                        <?php endif;?>
                        <li><input type="submit" value="inserisci nuova foto"/></li>
                    </ul>
                </form>
            </div>
            <?php if(isset($templateParams["aggiornamenti"]) && count($templateParams["aggiornamenti"])!=0):?>
            <aside>
                <h3>Aggiornamenti</h3>
                <ul>
                    <?php foreach($templateParams["aggiornamenti"] as $update): ?>
                        <li><?=$update?></li>
                    <?php endforeach;?>
                </ul>
            </aside>
            <?php endif;?>
        </div>
        <div class="tab-pane fade <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="motore")):?>
                    show active
                    <?php endif;?>" id="pills-motore" role="tabpanel" aria-labelledby="pills-motore-tab">
            <div>
                <h3>Inserisci un nuovo motore</h3>
                <form action="configuratore_venditore.php" method="POST" id="motore" enctype="multipart/form-data">
                    <input type="hidden" value="motore" name="tipo">
                    <input type="hidden" value="<?=$templateParams["auto"]["IdAuto"]?>" name="modello">
                    <ul>
                        <li><label for="m-nome">Nome</label></li>
                        <li><input type="text" id="m-nome" name="nome" required/></li>
                        <?php if(isset($templateParams["erroreMNome"])): ?>
                        <li><p><?=$templateParams["erroreMNome"]?></p></li>
                        <?php endif;?>
                        <li><label for="m-descrizione">Descrizione</label></li>
                        <li><textarea id="m-descrizione" name="descrizione" required></textarea></li>
                        <?php if(isset($templateParams["erroreMDescrizione"])): ?>
                        <li><p><?=$templateParams["erroreMDescrizione"]?></p></li>
                        <?php endif;?>
                        <li><label for="m-cilindrata">Cilindrata</label></li>
                        <li><input type="number" id="m-cilindrata" name="cilindrata" min="0" max="10000"/></li>
                        <?php if(isset($templateParams["erroreMCilindrata"])): ?>
                        <li><p><?=$templateParams["erroreMCilindrata"]?></p></li>
                        <?php endif;?>
                        <li><label for="m-cavalli">Cavalli</label></li>
                        <li><input type="number" id="m-cavalli" name="cavalli" min="0" max="10000"/></li>
                        <?php if(isset($templateParams["erroreMCavalli"])): ?>
                        <li><p><?=$templateParams["erroreMCavalli"]?></p></li>
                        <?php endif;?>
                        <li><label for="m-prezzo">Prezzo</label></li>
                        <li><input type="number" id="m-prezzo" name="prezzo" min="0" max="10000" required/></li>
                        <?php if(isset($templateParams["erroreMPrezzo"])): ?>
                        <li><p><?=$templateParams["erroreMPrezzo"]?></p></li>
                        <?php endif;?>
                        <li><input type="submit" value="inserisci nuovo motore"/></li>
                    </ul>
                </form>
            </div>
            <div >
                <section>
                    <h3>Motori presenti</h3>
                    <ul>
                    <?php foreach($templateParams["motori"] as $motore): ?>
                        <li>
                            <div>
                                <p><?=$motore["Nome"]?></p>
                                <p><?=$motore["Descrizione"]?></p>
                                <?php if($motore["Cilindrata"]!=null):?>
                                <p>Cilindrata: <?=$motore["Cilindrata"]?></p>
                                <?php endif;?>
                                <?php if($motore["Cavalli"]!=null):?>
                                <p>Cavalli: <?=$motore["Cavalli"]?></p>
                                <?php endif;?>
                                <p><em><?=$motore["Prezzo"]?></em>€</p>
                            </div>
                        </li>
                    <?php endforeach;?>
                    </ul>
                </section>
            </div>
        </div>
        <div class="tab-pane fade <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="esterni")):?>
                    show active
                    <?php endif;?>" id="pills-esterni" role="tabpanel" aria-labelledby="pills-esterni-tab">
            <div>
                <h3>Inserisci un nuovo esterno</h3>  
                <form action="configuratore_venditore.php" method="POST" id="esterni" enctype="multipart/form-data">
                    <input type="hidden" value="esterni" name="tipo">
                    <input type="hidden" value="<?=$templateParams["auto"]["IdAuto"]?>" name="modello">
                    <ul>
                        <li><label for="es-nome">Nome</label></li>
                        <li><input type="text" id="es-nome" name="nome" required/></li>
                        <?php if(isset($templateParams["erroreENome"])): ?>
                        <li><p><?=$templateParams["erroreENome"]?></p></li>
                        <?php endif;?>
                        <li>
                            <fieldset>
                                <legend>Tipo</legend>
                                <ul>
                                    <?php 
                                    $n=0;
                                    foreach($templateParams["esterniTitle"] as $title): ?>
                                        <li><input type="radio" id="e-<?=$title["Descrizione"]?>" name="tipologia" value="<?=$title["Descrizione"]?>"
                                        <?php if($n==0): ?>
                                            checked
                                        <?php 
                                        $n++;
                                        endif;?>
                                        required /><label for="e-<?=$title["Descrizione"]?>"><?=$title["Descrizione"]?></label></li>
                                    <?php endforeach;?>
                                </ul>
                            </fieldset>
                        </li>
                        <li><label for="es-nuovo">Nuovo tipo</label></li>
                        <li><input type="text" id="es-nuovo"/></li>
                        <li><input type="button" id="es-nuovoBotton" value="Aggiugi"/></li>
                        <li><label for="es-prezzo">Prezzo</label></li>
                        <li><input type="number" id="es-prezzo" name="prezzo" min="0" max="10000" required/></li>
                        <?php if(isset($templateParams["erroreEPrezzo"])): ?>
                        <li><p><?=$templateParams["erroreEPrezzo"]?></p></li>
                        <?php endif;?>
                        <li><label for="es-immagine">Inserisci immagine:</label></li>
                        <li><input type="file" id="es-immagine" name="immagine" accept="image/*"></li>
                        <?php if(isset($templateParams["erroreEImmagine"])): ?>
                        <li><p><?=$templateParams["erroreEImmagine"]?></p></li>
                        <?php endif;?>
                        <li><input type="submit" value="inserisci nuovo esterno"/></li>
                    </ul>
                </form>
            </div>
            <div >
                <h3>Esterni presenti</h3>  
                <?php foreach($templateParams["esterniTitle"] as $title): ?>
                    <section id="<?=$title["Descrizione"]?>-tipo">
                        <h4><?=$title["Descrizione"]?></h4>
                        <ul>
                            <?php foreach($templateParams["esterni"] as $elemento): ?>
                                <?php if($elemento["Descrizione"]==$title["Descrizione"]):?>
                                <li>
                                    <div>
                                        <?php if($elemento["Link_Immagine"]!=null): ?>
                                            <img src="<?=UPLOAD_DIR.$elemento["Link_Immagine"]?>" alt=""/>
                                        <?php endif; ?>
                                        <p><?=$elemento["Nome"]?></p>
                                        <p><em><?=$elemento["Prezzo"]?></em>€</p>
                                    </div>
                                </li>
                                <?php endif; ?>
                            <?php endforeach;?>
                        </ul>
                </section>
                <?php endforeach;?>
            </div>
        </div>
        <div class="tab-pane fade <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="interni")):?>
                    show active
                    <?php endif;?>" id="pills-interni" role="tabpanel" aria-labelledby="pills-interni-tab">
            <div>
                <h3>Inserisci un nuovo interno</h3>
                <form action="configuratore_venditore.php" method="POST" id="interni" enctype="multipart/form-data">
                    <input type="hidden" value="interni" name="tipo">
                    <input type="hidden" value="<?=$templateParams["auto"]["IdAuto"]?>" name="modello">
                    <ul>
                        <li><label for="in-nome">Nome</label></li>
                        <li><input type="text" id="in-nome" name="nome" required/></li>
                        <?php if(isset($templateParams["erroreINome"])): ?>
                        <li><p><?=$templateParams["erroreINome"]?></p></li>
                        <?php endif;?>
                        <li>
                            <fieldset>
                                <legend>Tipo</legend>
                                <ul>
                                    <?php 
                                    $n=0;
                                    foreach($templateParams["interniTitle"] as $title): ?>
                                        <li><input type="radio" id="i-<?=$title["Descrizione"]?>" name="tipologia" value="<?=$title["Descrizione"]?>"
                                        <?php if($n==0): ?>
                                            checked
                                        <?php 
                                        $n++;
                                        endif;?>
                                        required /><label for="i-<?=$title["Descrizione"]?>"><?=$title["Descrizione"]?></label></li>
                                    <?php endforeach;?>
                                </ul>
                            </fieldset>
                        </li>
                        <li><label for="in-nuovo">Nuovo tipo</label></li>
                        <li><input type="text" id="in-nuovo"/></li>
                        <li><input type="button" id="in-nuovoBotton" value="Aggiugi"/></li>
                        <li><label for="in-prezzo">Prezzo</label></li>
                        <li><input type="number" id="in-prezzo" name="prezzo" min="0" max="10000" required/></li>
                        <?php if(isset($templateParams["erroreIPrezzo"])): ?>
                        <li><p><?=$templateParams["erroreIPrezzo"]?></p></li>
                        <?php endif;?>
                        <li><label for="in-immagine">Inserisci immagine:</label></li>
                        <li><input type="file" id="in-immagine" name="immagine" accept="image/*"></li>
                        <?php if(isset($templateParams["erroreIImage"])): ?>
                        <li><p><?=$templateParams["erroreIImage"]?></p></li>
                        <?php endif;?>
                        <li><input type="submit" value="inserisci nuovo interno"/></li>
                    </ul>
                </form>
            </div>
            <div >
                <h3>Interni presenti</h3> 
                <?php foreach($templateParams["interniTitle"] as $title): ?>
                    <section id="<?=$title["Descrizione"]?>-tipo">
                        <h4><?=$title["Descrizione"]?></h4>
                        <ul>
                            <?php foreach($templateParams["interni"] as $elemento): ?>
                                <?php if($elemento["Descrizione"]==$title["Descrizione"]):?>
                                <li>
                                    <div>
                                        <?php if($elemento["Link_Immagine"]!=null): ?>
                                            <img src="<?=UPLOAD_DIR.$elemento["Link_Immagine"]?>" alt=""/>
                                        <?php endif; ?>
                                        <p><?=$elemento["Nome"]?></p>
                                        <p><em><?=$elemento["Prezzo"]?></em>€</p>
                                    </div>
                                </li>
                                <?php endif; ?>
                            <?php endforeach;?>
                        </ul>
                </section>
                <?php endforeach;?>
            </div>
        </div>
        <div class="tab-pane fade <?php if((isset($templateParams["actual"]) && $templateParams["actual"]=="optional")):?>
                    show active
                    <?php endif;?>" id="pills-optional" role="tabpanel" aria-labelledby="pills-optional-tab">
            <div>
                <h3>Inserisci un nuovo optional</h3>
                <form action="configuratore_venditore.php" method="POST" id="optional" enctype="multipart/form-data">
                    <input type="hidden" value="optional" name="tipo">
                    <input type="hidden" value="<?=$templateParams["auto"]["IdAuto"]?>" name="modello">
                    <ul>
                        <li><label for="op-nome">Nome</label></li>
                        <li><input type="text" id="op-nome" name="nome" required/></li>
                        <?php if(isset($templateParams["erroreONome"])): ?>
                        <li><p><?=$templateParams["erroreONome"]?></p></li>
                        <?php endif;?>
                        <li><label for="op-descrizione">Descrizione</label></li>
                        <li><textarea id="op-descrizione" name="descrizione" required></textarea></li>
                        <?php if(isset($templateParams["erroreODescrizione"])): ?>
                        <li><p><?=$templateParams["erroreODescrizione"]?></p></li>
                        <?php endif;?>
                        <li><label for="op-prezzo">Prezzo</label></li>
                        <li><input type="number" id="op-prezzo" name="prezzo" min="0" max="10000" required/></li>
                        <?php if(isset($templateParams["erroreOPrezzo"])): ?>
                        <li><p><?=$templateParams["erroreOPrezzo"]?></p></li>
                        <?php endif;?>
                        <li><label for="op-immagine">Inserisci immagine:</label></li>
                        <li><input type="file" id="op-immagine" name="immagine" accept="image/*"></li>
                        <?php if(isset($templateParams["erroreOImage"])): ?>
                        <li><p><?=$templateParams["erroreOImage"]?></p></li>
                        <?php endif;?>
                        <li><input type="submit" value="inserisci nuovo optional"/></li>
                    </ul>
                </form>
            </div>
            <div>
                <section>
                    <h3>Optional presenti</h3>
                    <ul>
                        <?php foreach($templateParams["optional"] as $elemento):?>
                        <li>
                            <div>
                                <?php if($elemento["Link_Immagine"]!=null): ?>
                                    <img src="<?=UPLOAD_DIR.$elemento["Link_Immagine"]?>" alt=""/>
                                <?php endif; ?>
                                <p><?=$elemento["Nome"]?></p>
                                <p><?=$elemento["Descrizione"]?></p>
                                <p><em><?=$elemento["Prezzo"]?></em>€</p>
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </section>
            </div>
        </div>
    </div>
    <?php endif ?>
</div>