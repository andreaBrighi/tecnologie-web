		<div class="container-fluid p-0 overflow-hidden">
			<div class="row">
				<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
				    <form action="#" method="POST" class="border m-4 p-4">
				        <h2 class="text-center pb-4">Ripristino account</h2>
				        <?php if(isset($templateParams["errorePassPersa"])): ?>
			            <p><?php echo $templateParams["errorePassPersa"]; ?></p>
			            <?php endif; ?>
				        <div class="form-group row">
				        	<div class="col-12">
					        	<fieldset>
					        		<legend>Tipologia account:</legend>
									<div class="col-12 btn-group btn-group-toggle" data-toggle="buttons">
										<label class="btn btn-primary active" for="cliente">
											<input type="radio" name="options_utente" id="cliente" value="Cliente" checked/> Cliente
										</label>
										<label class="btn btn-primary" for="venditore">
											<input type="radio" name="options_utente" id="venditore" value="Venditore"/> Venditore
										</label>
										<label class="btn btn-primary" for="corriere">
											<input type="radio" name="options_utente" id="corriere" value="Corriere"/> Corriere
										</label>
									</div>
								</fieldset>
							</div>
						</div>
				        <div class="form-group row" id="Field_CF">
				            <label for="cf" class="col-12">CF:</label>
				            <div class="col-1"></div>
				            <input type="text" id="cf" name="cf" class="form-control col-10" placeholder="CF" pattern=".{16,16}" title="16 caratteri" maxlength="16" required/>
				        </div>
				        <div class="form-group row" id="Field_PIVA">
				            <label for="P_IVA" class="col-12">P_IVA:</label>
				            <div class="col-1"></div>
				            <input type="text" id="P_IVA" name="P_IVA" class="form-control col-10" placeholder="P_IVA" pattern=".{11,11}" title="11 caratteri" maxlength="11" required/>
				        </div>
				        <div class="form-group row" id="Field_Email">
				            <label for="email" class="col-12">Email:</label>
				            <div class="col-1"></div>
				            <input type="email" id="email" name="email" class="form-control col-10" placeholder="Email" maxlength="140" required/>
				        </div>
				        <div class="form-group row" id="Field_NomUtente">
				            <label for="nomeutente" class="col-12">Nome Utente:</label>
				            <div class="col-1"></div>
				            <input type="text" id="nomeutente" name="nomeutente" class="form-control col-10" placeholder="Utente" maxlength="80" required/>
				        </div>
				        <div class="form-group text-right">
				            <input type="submit" name="submit" value="Ripristina password" class="btn btn-primary"/>
				        </div>
				    </form>
				</div>
			</div>
		</div>