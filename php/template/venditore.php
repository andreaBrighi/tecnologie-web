<a id="top" href="#header" aria-label="torna in alto"><em class='far fa-arrow-alt-circle-up' aria-hidden="true"></em></a>
    <div class="container-fluid">
        <header id="header" class="masthead">
            <a href="#tab" class="btn js-scroll-trigger" aria-label="vai al contenuto"><em class='fas fa-angle-double-down' aria-hidden="true"></em></a>
            <div>
                <img src="<?=UPLOAD_DIR.$templateParams["modelli"][0]["Link_ImmCopertina"]?>" alt="<?=$templateParams["titoloPagina"]?>"/>
            </div>
        </header>
        <div id="tab" >
            <ul class="nav nav-pills nav-justified" id="nav-pill" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="nav-modello-pill" data-toggle="pill" href="#nav-modello" role="tab"
                        aria-controls="nav-modello" aria-selected="true">Modello</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="nav-recensione-pill" data-toggle="pill" href="#nav-recensione" role="tab"
                        aria-controls="nav-recensione" aria-selected="false">Recensione</a>
                </li>
            </ul>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-modello" role="tabpanel" aria-labelledby="nav-modello-pill">
                    <h2>Modelli</h2>
                    <form>
                        <input type="hidden" id="CF" value="<?php 
                        if(isUserLoggedIn() && $_SESSION["CF"] != ""){
                            echo $_SESSION["CF"];
                        }?>"/>
                    </form>
                    <?php foreach($templateParams["modelli"] as $auto): ?>
                    <div>
                        <form>
                            <label for="<?=$auto["IdAuto"]?>">
                                <?=$auto["Modello"]?>
                            </label>
                            <button id="<?=$auto["IdAuto"]?>"><em class="material-icons <?php 
                            if(!is_null($auto["CF"])){
                                echo "red"; 
                            }?>"><?php 
                            if(is_null($auto["CF"])){
                                echo "favorite_border";
                            }
                            else{
                                echo "favorite";
                            }?></em></button>
                        </form>
                        <img src="<?=UPLOAD_DIR.$auto["Link_immagine"]?>" alt="<?=$auto["Modello"]?>" /><a href="visualizzazione_auto.php?idAuto=<?=$auto["IdAuto"]?>" class="btn btn-primary">
                            Più informazioni
                        </a>
                    </div>
                    <?php endforeach; ?>
                </div>
                <div class="tab-pane fade" id="nav-recensione" role="tabpanel" aria-labelledby="nav-recensione-pill">
                    <h2>Recensioni</h2>
                    <?php if(count($templateParams["recensioni"])>0):?>
                    <p>totale: <?=$templateParams["media"][0]["Media"]?>/5</p>
                    <div id="demo" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                        <?php 
                        $id=1;
                        foreach($templateParams["recensioni"] as $recensione): ?>
                            <div id="div_<?=$id?>" class="carousel-item <?php
                             if ($id==1){echo "active";}
                              ?>">
                                <h3><?=$recensione["Modello"]?></h3>
                                <p><?=$recensione["NumStelline"]?>/5</p>
                                <p><?=$recensione["Nome_Utente"]?></p>
                                <p><?=substr($recensione["Testo"],0,300)?>
                                <?php if(strlen($recensione["Testo"])>=300):?> 
                                    <span>...</span><span><?=substr($recensione["Testo"],300)?></span></p>
                                    <form>
                                        <button class="btn btn-primary" id="button_<?=$id?>">Read more</button>
                                    </form>
                                <?php else:?>
                                </p>
                                <?php endif;
                                $id++;
                                ?>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        <a class="carousel-control-prev" href="#demo" data-slide="prev" aria-label="precedente">
                            <em class="carousel-control-prev-icon" aria-hidden="true"></em>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next" aria-label="successiva">
                            <em class="carousel-control-next-icon" aria-hidden="true"></em>
                        </a>
                        <?php else:?>
                            <p>recensioni non presenti</p>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
        <div >
            <h2>Dove trovarci</h2>
            <?php if(isset($templateParams["venditore"][0]["Via"]) && isset($templateParams["venditore"][0]["N_Civico"]) && isset($templateParams["venditore"][0]["Citta"]) && isset($templateParams["venditore"][0]["CAP"])):?>
            <p id="indirizzo"><?= $templateParams["venditore"][0]["Via"]." ".$templateParams["venditore"][0]["N_Civico"]." ".$templateParams["venditore"][0]["Citta"]." ".$templateParams["venditore"][0]["CAP"]?></p>
            <div id="googleMap"></div>
            <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1IVZY4t0Ps4n9tJMgd6_qjG7XFWW-6fQ&callback=initMap"></script>
            <?php endif;?>
            <p><a href="mailto:<?=$templateParams["venditore"][0]["Email"]?>"><em class="material-icons">mail</em><?= $templateParams["venditore"][0]["Email"]?></a></p>
            <a href="tel://<?= $templateParams["venditore"][0]["Telefono"]?>"><em class="material-icons">call</em> <?= $templateParams["venditore"][0]["Telefono"]?></a>
        </div>
    </div>