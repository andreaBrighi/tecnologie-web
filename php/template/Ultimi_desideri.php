        <div class="container-fluid p-0 overflow-hidden">
            <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
                <h2 class="col-12 text-center my-4">Ultimi 6 Desideri</h2>
                <?php foreach(array_chunk($templateParams["ultimi_desideri"], 2) as $autos): ?>
                <?php if (isset($autos[0])): ?>
                <div class="row mb-2">
                    <div class="card border-0 col-12">
                        <div class="row no-gutters mx-2">
                            <div class="col-6 my-auto">
                                <img class="card-img-top rounded center-block" src="<?php echo UPLOAD_DIR.$autos[0]['Link_immagine']; ?>" alt="Immagine modello macchina: <?php echo $autos[0]['Modello']; ?>" />
                            </div>
                            <div class="col-6 my-auto">
                                <div class="card-body text-center py-0 px-2">
                                    <h3 class="card-title mb-1 font-weight-bold"><?php echo $autos[0]["Modello"]; ?></h3>
                                    <p class="card-text mb-1">Prezzo: <?php echo $autos[0]["Prezzo_base"]; ?>€</p>
                                    <form action="configuratore.php" method="GET">
                                        <div class="form-group text-center my-2">
                                            <input type="hidden" aria-hidden="true" name="idAuto" value="<?php echo $autos[0]["IdAuto"]; ?>" />
                                            <input type="submit" name="Configura" value="Configura" class="btn btn-dark"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>              
                <?php endif ?>
                <?php if (isset($autos[1])): ?>
                <div class="row mb-2">
                    <div class="card border-0 col-12">
                        <div class="row no-gutters mx-2">
                            <div class="col-6 my-auto text-center">
                                <div class="card-body text-center py-0 px-2">
                                    <h3 class="card-title mb-1 font-weight-bold"><?php echo $autos[1]["Modello"]; ?></h3>
                                    <p class="card-text mb-1">Prezzo: <?php echo $autos[1]["Prezzo_base"]; ?>€</p>
                                    <form action="configuratore.php" method="GET">
                                        <div class="form-group text-center my-2">
                                            <input type="hidden" aria-hidden="true" name="idAuto" value="<?php echo $autos[1]["IdAuto"]; ?>" />
                                            <input type="submit" name="Configura" value="Configura" class="btn btn-dark"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-6 my-auto">
                                <img class="card-img-top rounded center-block" src="<?php echo UPLOAD_DIR.$autos[1]['Link_immagine']; ?>" alt="Immagine modello macchina: <?php echo $autos[1]['Modello']; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif ?>
                <?php endforeach; ?>
            </div>
        </div>