        <div class="container-fluid p-0 overflow-hidden">
	        <div class="row mt-2">
			    <div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
		            <form action="#" method="POST" name="Login" class="bg-white border m-4 p-4">
		                <h2 class="text-center pb-4">Car Shop</h2>
		                <?php if(isset($templateParams["errorelogin"])): ?>
			            <p><?php echo $templateParams["errorelogin"]; ?></p>
			            <?php endif; ?>
		                <div class="form-group row">
		                    <label for="username" class="col-md-5 col-12">CF / P_IVA:</label>
		                    <input type="text" id="username" name="username" class="form-control col-10 col-md-7 mx-auto" placeholder="Username" required/>
		                </div>
		                <div class="form-group row">
		                    <label for="password" class="col-md-5 col-12">Password:</label>
		                    <div class="input-group col-10 col-md-7 mx-auto px-0">
		                    	<input type="password" maxlength="30" pattern="(?=.*\d)(?=.*[!@#$%^&*])(?=.*?[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,30}" id="password" name="password" class="form-control" placeholder="Password" required/>
			                    <div class="input-group-prepend">
		                            <button type="button" id="showPassword" class="btn btn-light rounded">
		                            	<em aria-hidden="true" class="fas fa-eye" title="mostra password"></em>
		                            </button>
		                        </div>
		                    </div>
		                </div>
		                <div class="form-group text-right">
		                    <input type="submit" name="submit" value="Invia" class="btn btn-primary"/>
		                </div>
		            </form>
			    </div>
			</div>
			<div class="row">
				<div class="col-12 text-center">
					<h2>Problemi?</h2>
				</div>			
			    <div class="col-6">
			    	<form action="passwordPersa.php" method="POST">
				    	<div class="form-group text-right">
		                    <input type="submit" name="PasswordPersa" value="Password persa?" class="btn btn-dark"/>
		                </div>
	        		</form>
			    </div>
			    <div class="col-6">
			    	<form action="registrati.php" method="POST">
				    	<div class="form-group text-left">
		                    <input type="submit" name="Registrati" value="Registrati" class="btn btn-dark"/>
		                </div>
	                </form>
			    </div>
		    </div>
		</div>