<div class="container" id="containerNotifiche">
<form method="GET" action="gestione_notifiche.php">
    <div class="container-fluid">
        <div class="row align-items-center text-center">
            <div class="col col-sm-6">
                <label for="filtra"> Filtra notifiche </label>
                <select class="form-control form-control-sm" id="filtra" name="filtra">
                    <option>Tutte</option>
                    <option>Lette</option>
                    <option>Non lette</option>
                </select>
            </div>
            <div class="col col-sm-6">
                <input type="submit" class="btn bg-light" value="Filtra"/>
            </div>
        </div>
    </div>
</form>

<?php if(isset($templateParams["Notifiche"])): ?>
<?php if(isset($templateParams["msg"])): ?>
<div class="alert alert-success" role="alert">
    <?php echo $templateParams["msg"]; ?>
</div>
<?php endif; ?>
<?php
    foreach($templateParams["Notifiche"] as $notifica):
?>

<div class="container-fluid bg-light mb-2">
    <form method="GET" action="gestione_notifiche.php">
        <div class="row" style="background-color: rgb(242, 242, 242);">
            <div class="col col-sm-6">
                <strong><?php echo $notifica["Titolo"]; ?></strong>
            </div>
            <div class="col col-sm-6">
                <p class="text-right">
                    <?php
                        if($notifica["Letto"] == 1){
                            echo "Letto";
                        } else {
                            echo "Non letto";
                        }
                    ?>
                </p>
            </div>
        </div>
        <div class="pb-3">
        <?php echo $notifica["Messaggio"]; ?>
        </div>
        <input type="hidden" aria-hidden="true" name="idNotifica" value="<?php echo $notifica["IdNotifica"]; ?>"/>
        <?php if($notifica["Letto"] == 0): ?>
        <input type="submit" class="btn btn-outline-info" value="Segna come letto"/>
        <?php endif;?>
    </form>
</div>

<?php endforeach; ?>
<?php else: ?>
<?php if(isset($templateParams["msg"])): ?>
<div class="alert alert-danger" role="alert">
    <?php echo $templateParams["msg"]; ?>
</div>
<?php endif; ?>
<?php endif; ?>
</div>