		<div class="container-fluid p-0 overflow-hidden">
			<div class="row">
				<div class="col-12">
					<h2 class="text-center mt-2 mb-4">Gestione</h2>
					<div class="row">
						<div class="col-6 text-right">
							<form action="manage_account.php" method="POST">
								<div class="form-group mb-0">
				                    <input type="submit" name="btnFatturazione" value="Fatturazione" class="btn btn-dark rounded"/>
				                </div>
							</form>
						</div>
						<div class="col-6 text-left">
							<form action="spedizione.php" method="POST">
								<div class="form-group mb-0">
				                    <input type="submit" name="btnSpedizioni" value="Spedizioni" class="btn btn-dark rounded"/>
				                </div>
							</form>						
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-4 mx-auto">
		            <form action="#" method="POST" class="bg-white border mt-5 mx-3 px-4 py-3">
		            	<h2 class="text-center mb-3">Pagamento</h2>
		            	<?php if(isset($templateParams["errorePagamento"])): ?>
			            <p><?php echo $templateParams["errorePagamento"]; ?></p>
			            <?php endif; ?>
			            <div class="form-group row" id="Field_Corriere">
				            <label for="corriere" class="col-12">Corriere:</label>
				            <select id="corriere" name="corriere" class="form-control mx-auto col-10">
				            	<?php foreach($templateParams["corrieri"] as $corriere): ?>
								<option value="<?php echo $corriere["P_IVA"]; ?>"><?php echo $corriere["Nome_Utente"]; ?></option>
				                <?php endforeach; ?>
							</select>
				        </div>
			            <div class="form-group row" id="Field_Spedizione">
				            <label for="spedizione" class="col-12">Spedizione:</label>
				            <select id="spedizione" name="spedizione" class="form-control mx-auto col-10">
				            	<?php foreach($templateParams["infoSpedizione"] as $spedizione): ?>
								<option value="<?php echo $spedizione["IdSpedizione"]; ?>"><?php echo $spedizione["Via"]."; ".$spedizione["N_Civico"]."; ".$spedizione["Citta"]."; ".$spedizione["Provincia"]."; ".$spedizione["CAP"]; ?></option>
				                <?php endforeach; ?>
							</select>
				        </div>
				        <div class="form-group row" id="Field_Fatturazione">
				        	<fieldset>
					            <legend class="col-12">Fatturazione:</legend>
					            <label class="radio-inline ml-5" for="fatturazione1">
									<input type="radio" name="fatturazione" id="fatturazione1" value="0" <?php if(isset($templateParams["infoFatturazione"]) && !$templateParams["infoFatturazione"]){echo "checked";} ?> /> NO
								</label>
								<label class="radio-inline ml-5" for="fatturazione2">
									<input type="radio" name="fatturazione" id="fatturazione2" value="1" <?php if(isset($templateParams["infoFatturazione"]) && $templateParams["infoFatturazione"]){echo "checked";} else {echo "disabled";}?> /> Sì
								</label>
							</fieldset>
				        </div>
				        <div class="form-group row" id="Field_PrezzoTotale">
				            <label for="PrezTot" class="col-12">Prezzo totale:</label>
				            <div class="col-1"></div>
				            <input class="form-control col-10" type="number" name="PrezTot" id="PrezTot" value="<?php if(isset($templateParams["infoPrezzoTotale"])){echo $templateParams["infoPrezzoTotale"];} ?>" disabled/>
				        </div>
		                <div class="form-group text-right">
		                    <input type="submit" name="submitPaga" value="Paga" class="btn btn-primary"/>
		                </div>
		            </form>
			    </div>
			</div>
		</div>