<div class="modal fade" id="offerta" tabindex="-1" style="z-index:3000;" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <label for="txtImporto" class="modal-title" id="exampleModalLabel">Fai un'offerta</label>
        <button type="button" class="close" id="closeModal" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
          <div class="row">
            <div class="col">
              <p id="prezzoAttualeModal"> Offerta attuale: <?php echo $templateParams["infoAsta"][0]["prezzoAttuale"]; ?> € </p>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="input-group mb-3">
                <input type="text" id="txtImporto" placeholder="Importo" class="form-control">
                <span class="input-group-text">€</span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="alert alert-primary" id="risultato" role="alert">
                Effettua un rilancio
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnEsegui" class="btn btn-primary">Esegui</button>
        <button type="button" class="btn btn-secondary" id="ChidiAsta" data-dismiss="modal">Chiudi</button>
      </div>
    </div>
  </div>
</div>