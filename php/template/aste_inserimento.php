<div class="container-fluid" id="inserimentoAste">
    <form action="aste.php" method="POST" enctype="multipart/form-data">
        <label for="nomeAuto" class="form-label">Nome auto</label>
        <input type="text" class="form-control" name="nomeAuto" id="nomeAuto" placeholder="" required/>
        <label for="descrizione" class="form-label">Descrizione</label>
        <textarea class="form-control" name="descrizione" id="descrizione" rows="5" required></textarea>
        <label for="immagine1">Immagine 1:</label>
        <input type="file" name="immagine1" id="immagine1" class="form-control border-0" required />
        <label for="immagine2">Immagine 2:</label>
        <input type="file" name="immagine2" id="immagine2" class="form-control border-0" required />
        <label for="immagine3">Immagine 3:</label>
        <input type="file" name="immagine3" id="immagine3" class="form-control border-0" required />
        <label for="immagine3">Suono:</label>
        <input type="file" name="suono" id="suono" class="form-control border-0" required />
        <label for="immagine3">Video:</label>
        <input type="file" name="video" id="video" class="form-control border-0" required />
        <div class="row">
            <div class="col-12">
                <label for="dataImm" class="form-label">Data Immatricolazione:</label>
                <input type="date" id="dataImm" name="dataImm" required/>
            </div>
        </div>
        
        <label for="prezzo" class="form-label">Prezzo iniziale</label>
        <input type="text" class="form-control" name="prezzo" id="prezzo" placeholder="" required/>
        <input type="submit" value="Registra" class="btn btn-primary m-2"/>
    </form>
</div>